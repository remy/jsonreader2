<?php


class ReaderTableDBQuery
{

    public static function newFromRow($row) {
        if($row != NULL) {
            return new ReaderTable(
                $row['ID'],
                utf8_encode($row['name']),
                utf8_encode($row['title']),
                utf8_encode($row['head']),
                $row['pos'],
                utf8_encode($row['jssort']));
        } else {
            return new ReaderTable(" - ", " - ", " - ", " - ", 9999, "");
        }
    }

    public static function getDefaultTableRead(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT name FROM ReaderTable WHERE 1 ORDER BY `ReaderTable`.`pos` ASC LIMIT 1;");
        while($row = mysqli_fetch_assoc($result)){
            return $row["name"];
        }
        
        return "";
    }

    public static function getAllReaderTable(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT * FROM ReaderTable ORDER BY pos;");
        $results = array();

        while($row = mysqli_fetch_assoc($result)) {
            $cr = ReaderTableDBQuery::newFromRow($row);
            //$cr->head = explode( ";", $cr->head);
            array_push($results, $cr);           
        }
        return $results;
    }

    public static function getNextPosReaderTable(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT MAX(`pos`) as maxpos FROM `ReaderTable`;");
        while($row = mysqli_fetch_assoc($result)) {
            $max = $row['maxpos'];
            return $max + 1;
        }
        return 0;
    }

    public static function getReaderTableWithName(DBquery $db, $name) {
        $result = mysqli_query($db->dbh, "SELECT * FROM ReaderTable WHERE name = '$name' ORDER BY pos;");
        while($row = mysqli_fetch_assoc($result)) {
            return ReaderTableDBQuery::newFromRow($row);
        }
        return new ReaderTable(" - ", " - ", " - ", " - ", 9999, "");
    }
    public static function getReaderTableByID(DBquery $db, $ID) {
        $result = mysqli_query($db->dbh, "SELECT * FROM ReaderTable WHERE ID = '$ID';");
        while($row = mysqli_fetch_assoc($result)) {
            return ReaderTableDBQuery::newFromRow($row);
        }
        return new ReaderTable(" - ", " - ", " - ", " - ", 9999, "");
    }
    
    public static function createReaderTable(DBquery $db, ReaderTable $rt) {
        $result = mysqli_query($db->dbh, $rt->getInsert());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }

    public static function updateReaderTable(DBquery $db, ReaderTable $rt) {
        $result = mysqli_query($db->dbh, $rt->getUpdate());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }

    public static function deleteReaderTable(DBquery $db, ReaderTable $rt) {
        $result = mysqli_query($db->dbh, $rt->getDelete());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }


}
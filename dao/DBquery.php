<?php

require_once(__DIR__.'/../config/Conf.php');
require_once(__DIR__.'/../config/Plugin.php');

require_once(__DIR__ .'/../model/Option.php');
/* plugins */
require_once(__DIR__ .'/../model/ReaderTable.php');
require_once(__DIR__ .'/../model/CReader.php');
require_once(__DIR__ .'/../model/CReaderPanel.php');
/* end plugins */

/**
 * Class for connection to database and requests
 **/
class DBquery {

    var $dbh = NULL;

    /**
     * Default constructor for DBquery
     */
    public function __construct() {
        $this->openConnection();
    }

    /**
     * For open a database connection
     **/
    public function openConnection() {
        $this->dbh = mysqli_connect(Conf::$DB_HOSTNAME, Conf::$DB_USERNAME, Conf::$DB_PP, Conf::$DB_NAME);

        if(!$this->dbh){
            die('Could not connect: ' . mysqli_error($this->dbh));
        }

        if (mysqli_connect_errno()) {
            printf("Echec connection : %s\n", mysqli_connect_error());
            exit();
        }
    }

    /**
     * Escapes special characters in a string for use in an SQL statement
     * 
     * @param string $value The value to escape
     * 
     * @return string
     */
    public function escape($value) {
        return mysqli_real_escape_string($this->dbh, $value);
    }

    /**
     * Verify the login and pass for admin
     * 
     * @param string $username The username to check
     * @param string $pass The pass to check
     * 
     * @return int
     */
    public function verifyPass($username, $pass) {
        if($username == Conf::$YII_USER && sha1($pass)==Conf::$YII_PP) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Generic function for create element in DB
     * 
     * @param Object $object The object to create
     * 
     * @return Message
     */
    public function create($object) {
        if(!JSONREADER2_STANDALONE) {
            $message = new Message("create", $object->getMessage("create"));
        }
        $result = mysqli_query($this->dbh, $object->getInsert());
        if($result) {
            if(isset($message)) {
                return $message; 
            } else { return ""; }
        } else {
            var_dump(mysqli_error($this->dbh));
            die();
        }
    }

    /**
     * Generic function for update element in DB
     * 
     * @param Object $object The object to create
     * 
     * @return Message
     */
    public function update($object) {
        if(!JSONREADER2_STANDALONE) {
            $message = new Message("update", $object->getMessage("update"));
        }
        $result = mysqli_query($this->dbh, $object->getUpdate());
        if($result) {
            if(isset($message)) {
                return $message; 
            } else { return ""; }
        } else {
            var_dump(mysqli_error($this->dbh));
            die();
        }
    }

    /**
     * Generic function for delete element in DB
     * 
     * @param Object $object The object to create
     * 
     * @return Message
     */
    public function delete($object) {
        if(!JSONREADER2_STANDALONE) {
            $message = new Message("delete", $object->getMessage("delete"));
        }
        $result = mysqli_query($this->dbh, $object->getDelete());
        if($result) {
            if(isset($message)) {
                return $message; 
            } else { return ""; }
        } else {
            var_dump(mysqli_error($this->dbh));
            die();
        }
    }

    /**
     * Get the number of element with table name & "id" field
     * 
     * @param string $table The name of the table
     * 
     * @return int
     */
    public function number($table) {
        $result = mysqli_query($this->dbh, "SELECT COUNT(id) as number FROM $table WHERE 1 ;");
        while($row = mysqli_fetch_assoc($result)){
            return $row['number'];
        }
        return 0;
    }

}


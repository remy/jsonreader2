<?php


class CReaderQuery
{

    public static function newFromRow($row) {
        if($row != NULL) {
            return new CReader(
                $row['ID'],
                $row['ckey'],
                $row['cvalue'],
                $row['FieldType'],
                $row['color']);
        } else {
            return new CReader(" - ", " - ", " - ", "text", "#FFFFFF");
        }
    }

    public static function getAllCReader(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReader ORDER BY ID;");
        $results = array();
        while($row = mysqli_fetch_assoc($result)) {
            $cr = CReaderQuery::newFromRow($row);
            $vx = explode( ";", $cr->cvalue);
            $results[$cr->ckey] = $vx;           
        }
        return $results;
    }

    public static function getAllCReaderObjects(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReader ORDER BY ID;");
        $results = array();
        while($row = mysqli_fetch_assoc($result)) {
            array_push($results, CReaderQuery::newFromRow($row));
        }
        return $results;
    }
    
    public static function getAllCReaderColor(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReader ORDER BY ID;");
        $results = array();
        while($row = mysqli_fetch_assoc($result)) {
            $cr = CReaderQuery::newFromRow($row);
            $results[$cr->ckey] = $cr->color;
        }
        return $results;
    }

    public static function getCReaderWithKey(DBquery $db, $ckey) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReader WHERE ckey = '$ckey' ORDER BY ID;");
        $results = array();
        while($row = mysqli_fetch_assoc($result)) {
            array_push($results, CReaderQuery::newFromRow($row));
        }
        return $results;
    }

    public static function getCReaderByID(DBquery $db, $ID) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReader WHERE ID = '$ID';");
        $results = array();
        while($row = mysqli_fetch_assoc($result)) {
            array_push($results, CReaderQuery::newFromRow($row));
        }
        return $results;
    }
    
    public static function updateCReader(DBquery $db, CReader $creader) {
        $result = mysqli_query($db->dbh, $creader->getUpdate());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }
    
    public static function createCReader(DBquery $db, CReader $creader) {
        $result = mysqli_query($db->dbh, $creader->getInsert());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }
    
    public static function deleteCReader(DBquery $db, CReader $creader) {
        $result = mysqli_query($db->dbh, $creader->getDelete());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }
}
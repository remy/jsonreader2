<?php


class CReaderPanelQuery
{

    public static function newFromRow($row) {
        if($row != NULL) {
            return new CReaderPanel(
                $row['ID'],
                $row['CReaderID'],
                utf8_encode($row['PanelTitle']),
                utf8_encode($row['PanelPosition']));
        } else {
            return new CReaderPanel(" - ", " - ", " - ", " - ");
        }
    }

    public static function getAllCReaderPanel(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReaderPanel ORDER BY PanelPosition;");
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            array_push($results, CReaderPanelQuery::newFromRow($row));
        }
        return $results;
    }

    public static function getOnlyPanelTitles(DBquery $db, $order="PanelTitle") {
        $result = mysqli_query($db->dbh, "SELECT DISTINCT PanelTitle FROM `CReaderPanel` ORDER BY ".$order." ASC;");
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            array_push($results, $row['PanelTitle']);
        }
        return $results;
    }

    public static function getCReaderPanelFromCReaderID(DBquery $db, $CReaderID) {
        $result = mysqli_query($db->dbh, "SELECT * FROM CReaderPanel WHERE CReaderID = '$CReaderID';");
        $results = array();
        while($row = mysqli_fetch_assoc($result)){
            array_push($results, CReaderPanelQuery::newFromRow($row));
        }
        return $results;
    }

    public static function updateCReaderPanel(DBquery $db, CReaderPanel $CReaderPanel) {
        $result = mysqli_query($db->dbh, $CReaderPanel->getUpdate());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }

    public static function getAllCReaderPanelByTitle(DBquery $db, $PanelTitle="") {
        $query = "SELECT * FROM `CReaderPanel`";
        if(!empty($PanelTitle)) {
            $endQuery = " WHERE PanelTitle='".$PanelTitle."';";
        } else {
            $endQuery = "';";
        }
        $query = $query . $endQuery;
        $result = mysqli_query($db->dbh, $query);
        $results = array();
        if($result) {
            while($row = mysqli_fetch_assoc($result)){
                array_push($results, CReaderPanelQuery::newFromRow($row));
            }
        }
        return $results;
    }

    public static function createCReaderPanel(DBquery $db, CReaderPanel $CReaderPanel) {
        $result = mysqli_query($db->dbh, $CReaderPanel->getInsert());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }

    public static function createCReaderPanelWithoutCReaderID(DBquery $db, CReaderPanel $CReaderPanel) {
        $result = mysqli_query($db->dbh, $CReaderPanel->getInsertWithoutCReaderID());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }
    
    public static function getNextPosCReaderPanel(DBquery $db) {
        $result = mysqli_query($db->dbh, "SELECT MAX(`PanelPosition`) as maxpos FROM `CReaderPanel`;");
        while($row = mysqli_fetch_assoc($result)){
            $max = $row['maxpos'];
            return $max + 1;
        }
        return 0;
    }
    
    public static function deleteCReaderPanel(DBquery $db, CReaderPanel $CReaderPanel) {
        $result = mysqli_query($db->dbh, $CReaderPanel->getDelete());
        if($result) {
            return "";
        } else {
            var_dump(mysqli_error($db->dbh));
            die();
        }
    }
        
    public static function deleteCReaderPanelByTitle(DBquery $db, $PanelTitle) {
        $Panels = CReaderPanelQuery::getAllCReaderPanelByTitle($db, $PanelTitle);
        foreach($Panels as $Panel) {
            $result = mysqli_query($db->dbh, $Panel->getDelete());
            if($result) {
                return "";
            } else {
                var_dump(mysqli_error($db->dbh));
                die();
            }
        }
    }

}
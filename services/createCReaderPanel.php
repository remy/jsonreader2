<?php

require_once "../config/Plugin.php";
define("PAGE","createCReaderPanel");


if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
    if($_SESSION['usercode'] != 1) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}

include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/include/tools.php";
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/CReaderPanelQuery.php";

$db  = new DBquery();
$creaderspanels = CReaderPanelQuery::getAllCReaderPanel($db);
$pos = CReaderPanelQuery::getNextPosCReaderPanel($db);

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1> Create a new Panel for CReader Config </h1>
        </div>
        <div class="col-lg-12">
            <form action="../action/actionJsonReaderPanel.php" method="post">

                <?php
                drawActionInputField("create");
                drawInputFormField("PanelTitle", "Panel Title", "", true, false);
                drawInputNumberFormField("position", "Position in config display", $pos, false, false);
                ?>

                <button type="submit" class="btn btn-primary">Create</button>
            </form>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>

<?php includeLibJS(); ?>

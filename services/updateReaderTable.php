<?php

require_once "../config/Plugin.php";
define("PAGE","updateReaderTable");


if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
    if($_SESSION['usercode'] != 1) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}

include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/include/tools.php";
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/ReaderTableDBQuery.php";

$db = new DBquery();

$ReaderTableid = $_GET['ReaderTableid'];

if(!isset($ReaderTableid) || empty($ReaderTableid)) {
    $ReaderTableid = -1;
}

$rt = ReaderTableDBQuery::getReaderTableById($db, $ReaderTableid);
//$rt->reclean();

echo '<!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1> Update ReaderTable </h1>
                </div>
                <div class="col-lg-12">
                    <form action="../action/actionReaderTable.php" method="post">';

                    drawActionInputField("update");
                    echo '<input type="text" class="form-control" id="ReaderTableID" placeholder="ReaderTableID" name="ReaderTableID" value="'.$rt->ID.'">';
                    drawInputFormField("name", "Filename suffix [1]", $rt->name, true, false);
                    drawInputFormField("title", "Title", $rt->title, true, false);
                    drawTextareaFormField("head", "Headers for table (&lt;th&gt;) (Main Fields)", $rt->head, false, false);
                    drawInputNumberFormField("pos", "Position", $rt->pos, false, false);
                    drawTextareaFormField("jssort", "JS Sort (DataTable options) [2]", $rt->jssort, false, false);
    
                    echo '<pre><p>';
                    echo "  [1] For example <b>_hosts_status</b> is the filename suffix for file <i>/var/www/html/exports/202109/01/20210901_0730<b>_hosts_status</b>.json</i>";
                    echo "
  [2] For example <b>order: [[3, 'desc']]</b> will sort the 3rd field from higher value to the lower (index starts at 0)
  see: https://datatables.net/examples/basic_init/table_sorting.html
  This field can also contain table length or whatever you want, included in JS function DataTables.
  Default is '\"pageLength\": -1, \"order\": [[1, \"asc\"]]'";
                    echo '</p></pre>';
                    
                        ?>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                    <br/><br/><br/><br/>
                </div>
            </div>
        </div>



<!-- jQuery Version 1.11.1 -->
<script src="https://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script >
            window.onload = function() {
               document.getElementById("ReaderTableID").style.display = "none";
               document.getElementById("action").style.display = "none";
           }
        </script>

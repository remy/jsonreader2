<?php

require_once "../config/Plugin.php";
define("PAGE","jsonreaderTable");

if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}
include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/ReaderTableDBQuery.php";
require_once JSR_PATH.'/include/tools.php';

$db = new DBquery();
$readerTable = ReaderTableDBQuery::getAllReaderTable($db);

?>

<div class="container container-mbb">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1> Json Reader Table </h1>
        </div>
        <div class="col-lg-12">

<?php

if(!JSONREADER2_STANDALONE) {
    if($_SESSION['usercode'] == 1) {
        echo '<a href="../services/createReaderTable.php" class="btn btn-success"> <i class="glyphicon glyphicon-plus"></i> </a><br/><br/>';
    }
} else {
    echo '<a href="../services/createReaderTable.php" class="btn btn-success"> <i class="glyphicon glyphicon-plus"></i> </a><br/><br/>';
}
echo '</div>
            <div class="col-lg-12 text-left">
                <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Reader Table</h3>
                    <div class="pull-right">
                        <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                            <i class="glyphicon glyphicon-filter"></i>
                        </span>
                    </div>
                </div>
                <div class="panel-body">
    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter ReaderTable" />
</div>
                  <table data-toggle="table" id="dev-table">
                    <thead>
                      <tr class="filters">
                        <th>Filename suffix (followed by .json)</th>
                        <th>Title</th>
                        <th>Position</th>
                        <th>Head (Main Fields)</th>
                        <th>JS Sort (DataTable options)</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>';

foreach($readerTable as $rt) {
    echo '<tr>';
    echo '<td>'.$rt->name.'</td>';
    echo '<td>'.$rt->title.'</td>';
    echo '<td>'.$rt->pos.'</td>';
    echo '<td><textarea class="form-control" row=5>'.$rt->head.'</textarea></td>';
    echo '<td><textarea class="form-control" row=5>'.$rt->jssort.'</textarea></td>';
    if(!JSONREADER2_STANDALONE) {
        if($_SESSION['usercode'] == 1) {
            echo '<td>
            <a href="../services/updateReaderTable.php?ReaderTableid=' . $rt->ID . '" class="btn btn-warning btn-sm"> <i class="glyphicon glyphicon-pencil"></i> </a>
            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteReaderTable" onclick="deleteModal(\'' . $rt->ID . '\', \'' . $rt->name . '\')"><i class="glyphicon glyphicon-remove"></i></button>
            </td>';
        } else {
            echo '<td></td>';
            echo '<td></td>';
        }
    } else {
        echo '<td>
        <a href="../services/updateReaderTable.php?ReaderTableid=' . $rt->ID . '" class="btn btn-warning btn-sm"> <i class="glyphicon glyphicon-pencil"></i> </a>
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteReaderTable" onclick="deleteModal(\'' . $rt->ID . '\', \'' . $rt->name . '\')"><i class="glyphicon glyphicon-remove"></i></button>
        </td>';
    }

    echo '</tr>';
}
echo '              </tbody>
                  </table>
                  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>

            </div>';
            drawModal("deleteReaderTable", "Suppression ReaderTable",
            '<p id="valueModal" >Vous allez supprimer de façon définitive le readertable : </p>',
            '<a id = "linkdelete" href="../action/actionReaderTable.php?action=delete&readertableid=" class="btn btn-danger btn-md"> Delete </a>'
            );
        echo '
        </div>
        <!-- /.row -->
    </div>';

    includeLibJS();
    echo '
    <script>
          function deleteModal(idReaderTable, nameReaderTable) {
              document.getElementById("valueModal").innerHTML = "Vous allez supprimer de façon définitive le readertable : \'" + nameReaderTable + "\'";
              document.getElementById("linkdelete").href = "../action/actionReaderTable.php?action=delete&ReaderTableID=" + idReaderTable;
          }
    </script>';
    ?>
</body>

</html>
<?php

require_once "../config/Plugin.php";
define("PAGE","createCReader");


if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
    if($_SESSION['usercode'] != 1) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}

include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/include/tools.php";
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/CReaderPanelQuery.php";

$db  = new DBquery();
$creaderspanels = CReaderPanelQuery::getAllCReaderPanel($db);
$creadersuniqpanels = CReaderPanelQuery::getOnlyPanelTitles($db);


?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1> Create CReader Config Item </h1>
        </div>
        <div class="col-lg-12">
            <form action="../action/actionJsonReader.php" method="post">

                <?php

                $data = array(new Option("text", "text"), new Option("textarea", "textarea"));
                drawActionInputField("create");
                drawInputFormField("ckey", "Json Key name (item name, without space)", "", true, false);
                drawInputFormField("cvalue", "Possible value(s) to parse [1]", "", true, false);
                drawSelectFormField("FieldType", "Field form type", $data, true);
                drawBasicColorPicker("DEFAULT_COLOR");
                $dataCpanel = [];
                echo '<p>Select an existing Panel:</p>';
                foreach($creadersuniqpanels as $cpaneltitle) {
                    array_push($dataCpanel, new Option($cpaneltitle, $cpaneltitle));
                }
                drawSelectFormField("Cpanel", "Cpanel", $dataCpanel, true, true);
                echo '<p>...Or create a <a href="./createCReaderPanel.php">new CPanel</a> first.</p>';

                echo '<p>[1] For multiple values, separate these values with ";" and then, select "textarea" as Field form type.</p>';
                ?>

                <button type="submit" class="btn btn-primary">Create</button>
            </form>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>

<?php
echo '<script src="https://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="../libjs/simplecolorpicker.js"></script>
';
    //includeLibJS();
?>
    <script>
    $(document).ready(function() {
      $('select[name="colorpicker-regularfont-DEFAULT_COLOR"]').simplecolorpicker({theme: 'regularfont'});
      $('select[name="colorpicker-regularfont-DEFAULT_COLOR"]').simplecolorpicker('selectColor', "#3498db");
    });
    </script>

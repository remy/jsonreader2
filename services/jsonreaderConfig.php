<?php

require_once "../config/Plugin.php";
define("PAGE","jsonreaderConfig");

if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}
include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/CReaderQuery.php";
require_once "../dao/CReaderPanelQuery.php";
require_once JSR_PATH."/include/tools.php";

$db = new DBquery();
$creaders = CReaderQuery::getAllCReader($db);
$allcreaderspanels = CReaderPanelQuery::getAllCReaderPanel($db);
$creadersColor = CReaderQuery::getAllCReaderColor($db);
$PanelTitles = [];

?>

<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1> Json Reader Config </h1>
        </div>
        <div class="col-lg-12text-left">
            <div class="btn-toolbar">

 <?php
 if(!JSONREADER2_STANDALONE) {
    if($_SESSION['usercode'] == 1) {
        echo '<a href="../services/createCReader.php" class="btn btn-success"> <i class="glyphicon glyphicon-plus"></i> </a>';
        echo '<a href="../services/createCReaderPanel.php" class="btn btn-success">Create a new Config Panel</a><br/><br/>';
    }
} else {
    echo '<a href="../services/createCReader.php" class="btn btn-success"> <i class="glyphicon glyphicon-plus"></i> </a>';
    echo '<a href="../services/createCReaderPanel.php" class="btn btn-success">Create a new Config Panel</a><br/><br/>';
}
?>
        </div>
        <form action="../action/actionJsonReader.php" method="post">
<?php
            drawActionInputField("save");
            foreach($allcreaderspanels as $crp) {
                array_push($PanelTitles, $crp->PanelTitle);
            }
            $uniqPanelTitles = array_unique($PanelTitles);
            foreach($uniqPanelTitles as $crptitle) {
                echo '
                <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">'.$crptitle.': </h3>
                    <div class="pull-right"><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteCRP" onclick="deleteModal(\'' . $crptitle . '\', \'' . $crptitle . '\')"><i class="glyphicon glyphicon-remove"></i></button></div>
                </div>
                <div class="panel-body">
                </div>
                <div class="row" style="padding: 20px;">';
                
                foreach($allcreaderspanels as $crp) {
                    $found = FALSE;
                    if($crptitle == $crp->PanelTitle) {
						echo "<table>";
                        foreach($creaders as $ckey => $cvalues) {
                            $current_cr_object = CReaderQuery::getCReaderWithKey($db, $ckey)[0];
                            if($crp->CReaderID == $current_cr_object->ID) {
                                $found = TRUE;
								echo "<tr><td>";
                                if($current_cr_object->FieldType == "text") {
                                    drawSelectColorPickerFormText($current_cr_object->ckey, $creaders);
                                } elseif($current_cr_object->FieldType == "textarea") {
                                    drawSelectColorPickerFormTextarea($current_cr_object->ckey, $creaders);
                                }
								echo "</td>";
                                echo '<td><div class="col-md-offset-3"><button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteCR" onclick="deleteModal(\'' . $current_cr_object->ID . '\', \'' . $current_cr_object->ckey . '\')"><i class="glyphicon glyphicon-remove"></i></button></div></td>';
							echo "</tr>";	
							//echo  "<hr class='hr-med-blue' />";
                            }
                        }
						echo "</table>";
                        if ($found == FALSE) {
                            echo '<strong>no config</strong>';
                        }
                    }
                }
                echo '</div>
                </div>';
            }

?>
            <button type="submit" class="btn btn-primary">Save</button>
            <br/><br/>
            </form>
        </div>
    </div>
</div>

<?php

drawModal("deleteCRP", "Delete Panel",
          '<p id="valueModalPanel" class="deletemodals">You will permanently delete this whole Config Reader Panel: </p>',
          '<a id="linkdeletePanel" href="../action/actionJsonReaderPanel.php?action=delete&PanelTitle=" class="btn btn-danger btn-md"> Delete </a>'
         );
drawModal("deleteCR", "Delete Config Item",
         '<p id="valueModalReader" class="deletemodals">You will permanently delete this Config Reader Item: </p>',
         '<a id="linkdeleteReader" href="../action/actionJsonReader.php?action=delete&CReaderID=" class="btn btn-danger btn-md"> Delete </a>'
        );
echo '<script src="https://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="../libjs/simplecolorpicker.js"></script>
';

?>

<script>

$(document).ready(function() {
    <?php
        foreach($creaders as $ckey => $cvalues) {
            echo "$('select[name=\"colorpicker-regularfont-".$ckey."\"]').simplecolorpicker({theme: 'regularfont'});";
            echo "$('select[name=\"colorpicker-regularfont-".$ckey."\"]').simplecolorpicker('selectColor', '".$creadersColor[$ckey]."');";
        }
        ?>

})
    

    function deleteModal(id, name) {
            document.getElementById('valueModalPanel').innerHTML = "You will permanently delete this whole Config Reader Panel:  '" + name + "'";
            document.getElementById('linkdeletePanel').href = "../action/actionJsonReaderPanel.php?action=delete&PanelTitle=" + id;
            document.getElementById('valueModalReader').innerHTML = "You will permanently delete this Config Reader Item:  '" + name + "'";
            document.getElementById('linkdeleteReader').href = "../action/actionJsonReader.php?action=delete&CReaderID=" + id;
    }
</script>

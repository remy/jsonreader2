<?php

require_once "../config/Plugin.php";
define("PAGE","jsonreader");

// uncomment the following if login is required to access this monitoring page
/*if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}*/
include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/CReaderQuery.php";
require_once "../include/toolReader.php";
require_once "../dao/ReaderTableDBQuery.php";
require_once "../model/SuperReader.php";
require_once JSR_PATH."/include/tools.php";

$db = new DBquery();

$creadersO = CReaderQuery::getAllCReaderObjects($db);
$reader = new SuperReader($creadersO);
$readersTable = ReaderTableDBQuery::getAllReaderTable($db);

$dt = new DateTimeImmutable("now", new DateTimeZone(Conf::$timezone));

?>

</head>

<script>

$( document ).ready(function() {

<?php

    $current_YYMMDD = $dt->format("Ymd");
    $current_YYMM   = $dt->format("Ym");
    $current_DD     = $dt->format("d");
    $current_HH     = $dt->format("H");
    $current_MM     = $dt->format("i");
    for($i=0; $i < 60 ; $i = $i + Conf::$step_minutes) {
        if(intval($current_MM) > $i) {$defaultM = $i; break;}
    } 

    $hh = "";
    $JSON_EXPORT_PATH = Conf::$export_dir;

    $defaultM = (strlen($defaultM) == 1) ? "0".$defaultM : $defaultM;
    //$current_HH = (strlen($current_HH) == 1) ? "0".$current_HH : $current_HH;
    // Access through GET/URL
    if(!isset($_POST["datechoose"]) && isset($_GET["data"])) {
        $_POST["datechoose"] = $current_YYMMDD;
        $_POST["datahour"] = $current_HH.$defaultM;
        $_POST["data"] = remove_special_chars($_GET["data"]);
    }
    // First page
    elseif(!isset($_POST["datechoose"]) || !isset($_POST["data"])) {
        $_POST["datechoose"] = $current_YYMMDD;
        $_POST["data"] = ReaderTableDBQuery::getDefaultTableRead($db);
        $_POST["datahour"] = $current_HH.$defaultM;
    }
   

    if(isset($_POST["datechoose"]) && isset($_POST["data"])) {
        if (empty($_POST['data'])) { $_POST["data"] = ReaderTableDBQuery::getDefaultTableRead($db); }
        $dateChoosen = $_POST["datechoose"];
        $arrdata = str_split($dateChoosen);
        $current_YYMM = $arrdata[0] . $arrdata[1] . $arrdata[2] . $arrdata[3] . $arrdata[4] . $arrdata[5];
        $current_DD = $arrdata[6] . $arrdata[7];
        $data = $_POST["data"];
        $filename = $JSON_EXPORT_PATH . $current_YYMM . "/" . $dateChoosen . $data . ".json";
        $hh = $_POST["datahour"];
    
        if(!file_exists($filename)) {
            // new config
            $filename = $JSON_EXPORT_PATH . $current_YYMM . "/". $current_DD . "/" . $dateChoosen . "_". substr($hh, 0, 3). "0" . $data . ".json";
            $indexName = 0;
            $find = false;
            if(!file_exists($filename)) {
                // trying Current time - 30mn to check if a previous file exists
                $new_hh = get_previous($current_HH, $current_MM, Conf::$step_minutes);
                $previous_filename = $JSON_EXPORT_PATH . $current_YYMM . "/". $current_DD . "/" . $dateChoosen . "_". substr($new_hh, 0, 3). "0" . $data . ".json";
                if(file_exists($previous_filename)) { $filename = $previous_filename; }
                if(!file_exists($filename)) {
                    $old_filename = check_files_from_older_versions($JSON_EXPORT_PATH, $current_YYMM, $current_DD, $dateChoosen, $hh, $data, $find);
                    if(file_exists($old_filename)) { $filename = $old_filename; }
                }
            }
        }
        echo 'console.log("'.$filename.'");';
        if (!file_exists($filename)) {
            echo 'alert("File not found: '.$filename.'");';
        }
        $string = file_get_contents($filename);
        $json = checkJsonError($string);
        $disks = false;

        //thead
        echo "var thead = $('thead');"."\n";
        $readerTable = ReaderTableDBQuery::getReaderTableWithName($db, $data);
        $serviceTitle = $readerTable->title;
        echo "thead.append('".$readerTable->getHTMLHead()."')"."\n";

        if($data == "_disks_status") {
            $json = $json["disks_status"];
        }

        // tbody
        echo "var tbody = $('tbody');"."\n";
        foreach ($json as $name => $val) {
            /*
            Corresponding function name is a CamelCase string of
            the json export filename with "parse" prefix and "Json" suffix.
            */
            $camelfunctionname = snake_case_to_CamelCase($data);
            $camelfunctionname = snake_case_to_CamelCase($readerTable->name);
            $camelfunctionname = "parse".$camelfunctionname."Json";
            if(function_exists($camelfunctionname)) {
                $camelfunctionname($name, $val, $reader);
            }
            elseif(!is_array($val) || $data == "_kernelversion" || $data == "_biosdate") {
                parseDefaultJson($name, $val, $reader);
            } else {
                $hostArray = $json[$name];
                if (!array_key_exists('ret', $hostArray)) {
                    /* Since saltstack 2016.11, everything is in 'ret' */
                    $passLoop = true;
                    foreach ($hostArray as $subname => $subval) {
                        if($data == "_websites_certs") {
                            if($passLoop) {
                                $passLoop = false;
                            } else {
                                $passLoop = true;
                                continue;
                            }
                        }
                        /* disk or service */
                        if(is_array($subval) && isset($subval)) {
                            //echo 'console.log("if------'.$data.'");';

                            /* service / daemon state */
                            /* module ... customservice.status...run: */
                            /* ret = {module...run:{}} */
                            parseServiceJsonArray($name, $subval, $reader);
                        }
                        else {
                            //echo 'console.log("else------'.$data.'");';
                            /* disk status */
                            /* ret = {} */
                            if($subname != "retcode") {
                                echo 'var tr = $("<tr>");'."\n";
                                $name = remove_quotes_and_new_line($name);
                                $subname = remove_quotes_and_new_line($subname);
                                $subval = remove_quotes_and_new_line($subval);
                                $sub2 = substr($subval, 0, -1);
                                $sub3 =  intval($sub2) + 10;
                                $sub4 = strval($sub3) . "%";
                                $color = "";
                                $colorStype = "";

                                if(intval($sub2) < $reader->DISKS_USAGE_WARNING) {
                                    $color = "progress-bar";
                                    $colorStype = "#5cb85c";
                                } 
                                elseif (intval($sub2) < $reader->DISKS_USAGE_DANGER) {
                                    $color = "progress-bar";
                                    $colorStype = $reader->COLOR_DISKS_USAGE_WARNING;
                                } else {
                                    $color = "progress-bar";
                                    $colorStype = $reader->COLOR_DISKS_USAGE_DANGER;
                                }

                                if($data == "_disks_status") {
                                    if($subval == $reader->DISKS_STATUS_OK) {
                                        $subval = '<span class="label" style="background-color: '.$reader->COLOR_DISKS_STATUS_OK.'">'.$subval.'</span>';
                                    } elseif($subval == $reader->DISKS_STATUS_PASSED) {
                                        $subval = '<span class="label" style="background-color: '.$reader->COLOR_DISKS_STATUS_PASSED.'">'.$subval.'</span>';
                                    } elseif(is_array($reader->DISKS_STATUS_PASSED) && in_array($subval, $reader->DISKS_STATUS_PASSED)) {
                                        $subval = '<span class="label" style="background-color: '.$reader->COLOR_DISKS_STATUS_PASSED.'">'.$subval.'</span>';
                                    } else {
                                        $subval = '<span class="label" style="background-color: #e74c3c">'.$subval.'</span>';
                                    }

                                } else {
                                    $subval =   '<div class="progress"><div class="'.$color.'" style="width: '.$sub4.'; background-color: '.$colorStype.'" role="progressbar" aria-valuenow="'.$sub3.'" aria-valuemin="0" aria-valuemax="150">'.$subval.'</div></div>';
                                }

                                if($data == "_disks") {
                                    if(substr($subname, 0, 5) != "/snap") {
                                        echo "$('<td>').html('".$name."').appendTo(tr);"."\n";
                                        echo "$('<td>').html('".$subname."').appendTo(tr);"."\n";
                                        echo "$('<td>').html('".$subval."').appendTo(tr);"."\n";
                                        echo 'tbody.append(tr);'."\n";
                                    }
                                }
                                else if($data == "_websites_certs") {
                                    $today = date("Y-m-d H:i:s");
                                    $subname = $json[$name]["enddate"];
                                    $date = $subname;
                                    $datediff = strtotime($date) - strtotime($today);
                                    $daysdiff = round($datediff / (60 * 60 * 24));
                                    if($daysdiff > $reader->WEBSITES_CERTS_SUCCESS) {
                                        $daysdiff = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITES_CERTS_SUCCESS.'">'.$daysdiff.'</span>';
                                    }
                                    else if($daysdiff > $reader->WEBSITES_CERTS_WARNING) {
                                        $daysdiff = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITES_CERTS_WARNING.'">'.$daysdiff.'</span>';
                                    } else {
                                        $daysdiff = '<span class="label" style="background-color: #e74c3c">'.$daysdiff.'</span>';
                                    }

                                    $subval = $json[$name]["status"];
                                    if($subval == $reader->WEBSITES_CERTS_STATUS_OK) {
                                        $subval = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITES_CERTS_STATUS_OK.'">'.$subval.'</span>';
                                    } else {
                                        $subval = '<span class="label" style="background-color: #e74c3c">'.$subval.'</span>';
                                    }

                                    $name = "<a href=\"https://".$name."\">".$name."</a>";
                                    echo "$('<td>').html('".$name."').appendTo(tr);"."\n";
                                    echo "$('<td>').html('".$subname."').appendTo(tr);"."\n";
                                    echo "$('<td>').html('".$daysdiff."').appendTo(tr);"."\n";
                                    echo "$('<td>').html('".$subval."').appendTo(tr);"."\n";
                                    echo 'tbody.append(tr);'."\n";
                                } else {
                                    echo "$('<td>').html('".$name."').appendTo(tr);"."\n";
                                    echo "$('<td>').html('".$subname."').appendTo(tr);"."\n";
                                    echo "$('<td>').html('".$subval."').appendTo(tr);"."\n";
                                    echo 'tbody.append(tr);'."\n";

                                }
                            }
                        }
                    }
                }
            }
        }
        echo "\n";
        echo "$('#datechoose').val('".$dateChoosen."');"."\n";
    }
    else {
        $data = "_hosts_status";
        echo '$("#datechoose").datepicker({dateFormat: "yymmdd"}).datepicker("setDate", new Date());'."\n";
    }
?>

    $( "#datechoose" ).datepicker({
        dateFormat: "yymmdd",
        defaultDate: null,
    });

    $('#myTable').DataTable({
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],

        <?php 

            $readerTable = ReaderTableDBQuery::getReaderTableWithName($db, $data);
            $jssort = $readerTable->jssort;
            if($jssort != NULL && $jssort != "") {
                echo $jssort;
            }
            else {
                echo '"pageLength": -1,
                "order": [[1, "asc"]]';
            }

        ?>
        });
});
</script>

    <script>
        
        jQuery.fn.dataTableExt.oSort["rank-asc"] = function (x, y) {
            if(y == "OK") {
                return 0;
            } else if (x == "OK") {
                return 0;
            } else {
                return x > y;  
            }
        }   

    </script>

</head>

<body>
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12 text-center">
            <?php foreach ($readersTable as $rd) {
                if ($rd->title == $serviceTitle) {
                $curName = $rd->name;
                }
            }
            echo "<h1><a href='jsonreader.php?data=".htmlspecialchars(urlencode($curName))."'> ".$serviceTitle."</a></h1>";
            echo "<h3>".getDateTimeTitle($filename)."</h3>"; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1">
            <form id="my_form" method='POST'>
                <div class="form-group">
                    <label for="datechoose">Select date [YYYYMMDD] : </label>
                    <input type="text" id="datechoose" name="datechoose" class="form-control" onchange="updateTime()"/>
                </div>
                <div class="form-group">
                <label for="datahour">Select hour : </label>
                    <select id="datahour" name="datahour" onchange="updateTime()">
                    <?php
                        printDataHourSelect($hh, Conf::$step_minutes);
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="btn-group-vertical" role="group" aria-label="test">
                    <?php 
                        foreach ($readersTable as $rd) {
                            if(in_array($rd->title, $SECTIONS)) {
                                echo '<button type="submit" name="data" value="'.$rd->name.'" class="btn btn-primary">'.$rd->title.'</button>';
                            }
                        }
                    ?>
                    </div>
                </div>
            </form>
            <br/>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalJSON"><i class="far fa-eye"></i>&nbsp;JSON </button>

        </div>

        <div class="col-md-11">

            <div id="results">
                <table id="myTable" class="perso table table-striped">
                    <thead class="thead-dark">
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModalJSON" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">JSON</h4>
                </div>
                <div class="modal-body">
                <textarea class="form-control" rows="23"><?php echo $string;?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

function updateTime() {
  var data = <?php echo '"' . $_POST["data"]. '"';  ?>;
  var form = document.getElementById("my_form");
  var input = document.createElement('input');
    input.setAttribute('name', "data");
    input.setAttribute('value', data);
    input.setAttribute('type', 'hidden');
    form.appendChild(input);
    form.submit();
}

</script>

</body>
</html>

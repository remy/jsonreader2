<?php

require_once "../config/Plugin.php";
define("PAGE","createReaderTable");


if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
    if($_SESSION['usercode'] != 1) {
        $_SESSION['lastpage'] = PAGE;
        header("Location: ".JSR_PATH."/services/login.php");
    }
}

include(JSR_PATH."/include/buildHeader.php");
require_once JSR_PATH."/include/tools.php";
require_once JSR_PATH."/dao/DBquery.php";
require_once "../dao/ReaderTableDBQuery.php";

$db  = new DBquery();
$pos = ReaderTableDBQuery::getNextPosReaderTable($db);

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1> Create ReaderTable </h1>
        </div>
        <div class="col-lg-12">
            <form action="../action/actionReaderTable.php" method="post">

                <?php

                drawActionInputField("create");
                drawInputFormField("name", "Filename suffix [1]", "", true, false);
                drawInputFormField("title", "Title", "", true, false);
                drawTextareaFormField("head", "Headers for table (&lt;th&gt;) (Main Fields)", "", false, false);
                drawInputNumberFormField("pos", "Position", $pos, false, false);
                drawTextareaFormField("jssort", "JS Sort (DataTable options) [2]", "", false, false);

                echo '<pre><p>';
                echo "  [1] For example <b>_hosts_status</b> is the filename suffix for file <i>/var/www/html/exports/202109/01/20210901_0730<b>_hosts_status</b>.json</i>";
                echo "
  [2] For example <b>order: [[3, 'desc']]</b> will sort the 3rd field from higher value to the lower (index starts at 0)
  see: https://datatables.net/examples/basic_init/table_sorting.html
  This field can also contain table length or whatever you want, included in JS function DataTables.
  Default is '\"pageLength\": -1, \"order\": [[1, \"asc\"]]'";
                echo '</p></pre>';

                ?>

                <button type="submit" class="btn btn-primary">Create</button>
            </form>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>


<?php
    
    includeLibJS();

?>
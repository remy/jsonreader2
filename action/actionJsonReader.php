<?php

require_once "../config/Plugin.php";
define("PAGE","actionJsonReader");

if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
}

require_once JSR_PATH.'/dao/DBquery.php';
require_once '../dao/CReaderQuery.php';
require_once '../dao/CReaderPanelQuery.php';
require_once "../model/CReader.php";
require_once "../model/CReaderPanel.php";
if(!JSONREADER2_STANDALONE) {
    require_once JSR_PATH.'/dao/LogDBQuery.php';
    require_once JSR_PATH.'/model/Message.php';
    require_once JSR_PATH.'/model/Log.php';
}

$db = new DBquery();

$action  = NULL;
$message = NULL;
$today = date("Y-m-d G:i:s");

$ID        = -1;
$ckey      = NULL;
$cvalue    = NULL;
$FieldType = NULL;
$color     = "#3498db"; // default is blue

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if($action == "save") {
    $valueReader = [];
    $colorReader = [];
    $FieldTypeReader = [];
    foreach ($_POST as $ckey => $cvalue){
        if(strpos($ckey, "colorpicker-regularfont-") !== false) {
            $colorReader[str_replace("colorpicker-regularfont-", "", $ckey)] = $cvalue;
        } elseif(strpos($ckey, "FieldType-") !== false) {
            $FieldTypeReader[str_replace("FieldType-", "", $ckey)] = $cvalue;
        } else {
            $valueReader[$ckey] = $cvalue;
        }
    }
    
    foreach ($valueReader as $ckey => $cvalue){
        $color = $colorReader[$ckey];
        $FieldType = $FieldTypeReader[$ckey];
        $value =  str_replace("\n", ";", $cvalue);
        $value =  str_replace("\r", "", $value);
        $creader = new CReader(0, $ckey, $value, $FieldType, $color);
        CReaderQuery::updateCReader($db, $creader);
    }
} else {
    
    if(isset($_POST['CReaderID'])) {
        $ID = $_POST['CReaderID'];
    } elseif(isset($_GET['CReaderID'])) {
        $ID = $_GET['CReaderID'];
    } else {
        $ID = -1;
    }
    if(isset($_POST['ckey'])) {
        $ckey = $_POST['ckey'];
    }
    if(isset($_POST['cvalue'])) {
        $cvalue = $_POST['cvalue'];
    }
    if(isset($_POST['FieldType'])) {
        $FieldType = $_POST['FieldType'];
    }
    if(isset($_POST['color'])) {
        $color = $_POST['color'];
    } elseif(isset($_POST['DEFAULT_COLOR'])) {
        $color = $_POST['DEFAULT_COLOR'];
    } elseif(isset($_POST['colorpicker-regularfont-DEFAULT_COLOR'])) {
        $color = $_POST['colorpicker-regularfont-DEFAULT_COLOR'];
    } else { $color = "#3498db"; }
    
    if(isset($_POST['Cpanel'])) {
        $CpanelTitle = $_POST['Cpanel'];
    }

    $creader = new CReader($ID, $ckey, $cvalue, $FieldType, $color);

    if($action == "create") {
        /* uncomment following and comment header Location at the end of this file bellow for debug */
        //var_dump($_POST);
        $creader->escape($db);
        $message = $db->create($creader);
        if(!JSONREADER2_STANDALONE) {
            $log = new Log(-1, "admin", $creader->ID, "insert", $message->value, $today, -1);
            LogDBQuery::createLog($db, $log);
        }

        $current_Creader = CReaderQuery::getCReaderWithKey($db, $ckey);

        if(!empty($CpanelTitle)) {
            $existingCPanels = CReaderPanelQuery::getAllCReaderPanelByTitle($db, $CpanelTitle);
            if(!empty($existingCPanels)) {
                if (count($existingCPanels) == 1 && is_null($existingCPanels[0]->CReaderID)) {
                    /* update the NULL (or maybe "NULL") value to the new $CReader->ID */
                    $existingCPanels[0]->CReaderID = $current_Creader[0]->ID;
                    $existingCPanels[0]->escape($db);
                    $message = $db->update($existingCPanels[0]);
                    if(!JSONREADER2_STANDALONE) {
                        $log = new Log(-1, "admin", $existingCPanels[0]->ID, "update", $message->value, $today, -1);
                        LogDBQuery::createLog($db, $log);
                    }
                } else {
                    $pos = $existingCPanels[0]->PanelPosition;
                    $creaderpanel = new CReaderPanel($ID, $current_Creader[0]->ID, $CpanelTitle, $pos);
                    $creaderpanel->escape($db);
                    $message = $db->create($creaderpanel);
                    if(!JSONREADER2_STANDALONE) {
                        $log = new Log(-1, "admin", $creaderpanel->ID, "insert", $message->value, $today, -1);
                        LogDBQuery::createLog($db, $log);
                    }
                }
            }
        }
    }
    if($action == "delete") {
        $current_Creader = CReaderQuery::getCReaderByID($db, $ID);
        $corresponding_panel = CReaderPanelQuery::getCReaderPanelFromCReaderID($db, $ID)[0];
        $PanelTitle = $corresponding_panel->PanelTitle;
        $message = CReaderPanelQuery::deleteCReaderPanel($db, $corresponding_panel);
        if(!JSONREADER2_STANDALONE) {
            $log = new Log(-1, "admin", $PanelTitle, "delete", $message->value, $today, -1);
            LogDBQuery::createLog($db, $log);
        }        
        $message = CReaderQuery::deleteCReader($db, $current_Creader[0]);
        if(!JSONREADER2_STANDALONE) {
            $log = new Log(-1, "admin", $ID, "delete", $message->value, $today, -1);
            LogDBQuery::createLog($db, $log);
        }
    }
}


header("Location: ../services/jsonreaderConfig.php");

?>
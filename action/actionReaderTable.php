<?php

require_once "../config/Plugin.php";
define("PAGE","actionReaderTable");

if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
}

require_once JSR_PATH.'/dao/DBquery.php';
require_once '../dao/ReaderTableDBQuery.php';
require_once "../model/ReaderTable.php";
if(!JSONREADER2_STANDALONE) {
    require_once JSR_PATH.'/dao/LogDBQuery.php';
    require_once JSR_PATH.'/model/Message.php';
    require_once JSR_PATH.'/model/Log.php';
}

$db = new DBquery();

$action  = NULL;
$message = NULL;
$today = date("Y-m-d G:i:s");

$ID        = -1;
$name      = NULL;
$title     = NULL;
$head      = NULL;
$pos       = 9999;
$jssort    = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['ReaderTableID'])) {
    $ID = $_POST['ReaderTableID'];
} else {
    if(isset($_GET['ReaderTableID'])) {
        $ID = $_GET['ReaderTableID'];
    } else {
        $ID = -1;
    }
}

if(isset($_POST['name'])) {
    $name = $_POST['name'];
}

if(isset($_POST['title'])) {
    $title = $_POST['title'];
}

if(isset($_POST['pos'])) {
    $pos = intval($_POST['pos']);
} else {
    $pos = ReaderTableDBQuery::getNextPosReaderTable($db);
}

if(isset($_POST['head'])) {
    $head = $_POST['head'];
}

if(isset($_POST['jssort'])) {
    $jssort = $_POST['jssort'];
}

$ReaderTable = new ReaderTable($ID, $name, $title, $head, $pos, $jssort);

if($action == "create") {
    $ReaderTable->escape($db);
    $message = ReaderTableDBQuery::createReaderTable($db, $ReaderTable);
    if(!JSONREADER2_STANDALONE) {
        $log = new Log(-1, "admin", $ReaderTable->name, "insert", $message->value, $today, -1);
        LogDBQuery::createLog($db, $log);
    }
} else if ($action == "update") {
    $ReaderTable->escape($db);
    $message =  ReaderTableDBQuery::updateReaderTable($db, $ReaderTable);
    if(!JSONREADER2_STANDALONE) {
        $log = new Log(-1, "admin", $ReaderTable->name, "update", $message->value, $today, -1);
        LogDBQuery::createLog($db, $log);
    }
} else if ($action == "delete") {
    $message = ReaderTableDBQuery::deleteReaderTable($db, $ReaderTable);
    if(!JSONREADER2_STANDALONE) {
        $log = new Log(-1, "admin", $ReaderTable->ID, "delete", $message->value, $today, -1);
        LogDBQuery::createLog($db, $log);
    }
}

header("Location: ../services/jsonreaderTable.php");
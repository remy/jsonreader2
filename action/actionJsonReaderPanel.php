<?php

require_once "../config/Plugin.php";
define("PAGE","actionJsonReader");

if(!JSONREADER2_STANDALONE) {
    session_start ();
    if(!isset($_SESSION['username'])) {
        header("Location: ".JSR_PATH."/index.php");
    }
}

require_once JSR_PATH.'/dao/DBquery.php';
require_once '../dao/CReaderPanelQuery.php';
require_once '../dao/CReaderQuery.php';
require_once "../model/CReaderPanel.php";
require_once "../model/CReader.php";
if(!JSONREADER2_STANDALONE) {
    require_once JSR_PATH.'/dao/LogDBQuery.php';
    require_once JSR_PATH.'/model/Message.php';
    require_once JSR_PATH.'/model/Log.php';
}

$db = new DBquery();

$action  = NULL;
$message = NULL;
$today = date("Y-m-d G:i:s");

$ID         = -1;
$PanelTitle = NULL;
$position   = NULL;
$CReaderID  = NULL;

if(isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    if(isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = "";
    }
}

if(isset($_POST['PanelID'])) {
    $ID = $_POST['PanelID'];
} else {
    if(isset($_GET['PanelID'])) {
        $ID = $_GET['PanelID'];
    } else {
        $ID = -1;
    }
}
if(isset($_POST['PanelTitle'])) {
    $PanelTitle = $_POST['PanelTitle'];
} else {
    if(isset($_GET['PanelTitle'])) {
        $PanelTitle = $_GET['PanelTitle'];
    }
}

if(isset($_POST['position'])) {
    $position = $_POST['position'];
}

$creaderpanel = new CReaderPanel($ID, $CReaderID, $PanelTitle, $position);

if($action == "create") {
    /* uncomment following and comment header Location at the end of this file bellow for debug */
    //var_dump($_POST);
    $creaderpanel->escape($db);
    $message = CReaderPanelQuery::createCReaderPanelWithoutCReaderID($db, $creaderpanel);
    //$message = $db->create($creaderpanel);
    if(!JSONREADER2_STANDALONE) {
        $log = new Log(-1, "admin", $creaderpanel->ID, "insert", $message->value, $today, -1);
        LogDBQuery::createLog($db, $log);

    }
}
if($action == "delete") {
    $CRs = CReaderQuery::getAllCReaderObjects($db);
    $Panels = CReaderPanelQuery::getAllCReaderPanelByTitle($db, $PanelTitle);
    foreach($Panels as $Panel) {
        foreach($CRs as $CR) {
            if($CR->ID == $Panel->CReaderID) {
                $message = CReaderQuery::deleteCReader($db, $CR);
                if(!JSONREADER2_STANDALONE) {
                    $log = new Log(-1, "admin", $CR->ckey, "delete", $message->value, $today, -1);
                    LogDBQuery::createLog($db, $log);
                }
            }
        }
    }
    $message = CReaderPanelQuery::deleteCReaderPanelByTitle($db, $PanelTitle);
    if(!JSONREADER2_STANDALONE) {
        $log = new Log(-1, "admin", $PanelTitle, "delete", $message->value, $today, -1);
        LogDBQuery::createLog($db, $log);
    }
}


header("Location: ../services/jsonreaderConfig.php");

?>

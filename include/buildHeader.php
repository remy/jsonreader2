<?php

if(!isset($init) || $init != true){
    include(JSR_PATH.'/include/init.php');
}



echo  '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">

        <html xmlns="https://www.w3.org/1999/xhtml" xml:lang=\'fr\'

          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="description" content="">
          <meta name="author" content="">

          <title>Jsonreader2</title>

          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
          <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
          <link rel="stylesheet" type="text/css" href="../css/mycss.css" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" />
          <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css" rel="stylesheet" />
          <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
          <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />    
          <link rel="stylesheet" href="../css/simplecolorpicker.css" />
          <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
          <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
        ';

        if(PAGE == "jsonreader") {
            echo '
            <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/smoothness/jquery-ui.css" />
            <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4/dt-1.13.6/datatables.min.css" />
            <link rel="stylesheet" href="../css/jsonreader.css">
            <script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"></script>
            <script src="https://cdn.datatables.net/v/bs4/dt-1.13.6/datatables.min.js"></script>';
        }

echo '
          </head>
          <body>';


echo '<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--a class="navbar-brand" href="/'.Conf::$root_dir.'/index.php">Portail admin</a-->
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">';

            echo '
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Configuration
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/'.Conf::$root_dir.'/services/jsonreaderConfig.php"> Jsonreader Colors</a></li>
                            <li><a href="/'.Conf::$root_dir.'/services/jsonreaderTable.php"> Jsonreader Table Config</a></li>
                        </ul>
                </li>
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reporting
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/'.Conf::$root_dir.'/services/jsonreader.php"> Jsonreader </a></li>
                    </ul>
            </li>                                             
            </ul>';

            

 echo '           
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>';

?>

<?php
/**
 * Created by PhpStorm.
 * User: jimmy
 * Date: 12/03/18
 * Time: 14:01
 */

/**
 * @param string $str
 * @param string $charset
 * @return mixed|string
 */
function remove_accents_and_space_and_smaller(string $str, $charset='utf-8') {
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    $str = strtolower($str);
    $str = strtr($str, ' ', '_');
    return $str;
}

function snake_case_to_CamelCase($string) {
    $str2array = explode("_", $string);
    $newarray = array();
    foreach($str2array as $str) {
        array_push($newarray, ucfirst($str));
    }
    return(implode("", $newarray));
}

function drawPanel($title, $value, $url) {
    echo '
    <div class="col-lg-4 ">
        <div class="panel">
            <div class="bg-index">
                <span class="glyphicon glyphicon-list-alt icon-index" ></span>
                <div class="text-index">
                    '.$title.'
                    <br/>
                    <div class="value-index">
                        ' . $value . '
                    </div>
                </div>
            </div>
            <div class="botton-index">
                <div class="button-index">
                    <a href="/' . $url . '" class="btn btn-primary">View</a>
                </div>
            </div>
        </div>
    </div>
    ';
}

function drawFormField($name, $value) {
    echo '
    <div class="form-group">
        <label class="text-right col-md-6 control-label" for="nome">'.$name.' :</label>
        <div class="col-md-6" >
            <p>'.$value.'</p>      
        </div>
    </div>

    ';
}

function drawFormFieldWithLDAP($name, $value, $ldap) {
    echo '
    <div class="form-group">
        <label class="text-right col-md-6 control-label" for="nome">'.$name.' :</label>
        <div class="col-md-6" >';
        if($value == $ldap) {
            echo '<p>'.$value.' '.'<button type="button" class="btn btn-success btn-xs">ldap ok</button></p>';
        } else {
            echo '<p>'.$value.' '.'<button type="button" class="btn btn-danger btn-xs">ldap error : '.$ldap.'</button></p>';
        } 
        echo'    
        </div>
    </div>

    ';
}

function drawModal($id, $title, $body, $footer) {
    echo '
    <!-- Modal -->
    <div id="'.$id.'" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">'.$title.': </h4>
          </div>
          <div class="modal-body">
            '.$body.'
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Close</button>
            '.$footer.'
          </div>
        </div>

      </div>
    </div>
    <!-- End Modal -->
    ';
}

function drawInputFormField($name, $title, $value, $required, $readonly) {
    $star = "";
    $rq = "";
    $rd = "";
    if($required) {
        $star = '<span style="font-size:26px;" class="status text-danger">*</span>';
        $rq = 'required="true"';
    }
    if($readonly) {
        $rd = 'readonly="readonly"'; 
    }
    echo '
        <div class="form-group">
            <label for="'.$name.'">'.$title.' : '.$star.'</label>
            <input type="text" class="form-control" id="'.$name.'" placeholder="'.$name.'" value="'.$value.'" name="'.$name.'" '.$rq.' '.$rd.' >
        </div>
    ';
}

function drawInputNumberFormField($name, $title, $value, $required, $readonly) {
    $star = "";
    $rq = "";
    $rd = "";
    if($required) {
        $star = '<span style="font-size:26px;" class="status text-danger">*</span>';
        $rq = 'required="true"';
    }
    if($readonly) {
        $rd = 'readonly="readonly"'; 
    }
    echo '
    <div class="form-outline">
        <label for="'.$name.'">'.$title.' : '.$star.'</label>
        <input type="number" class="form-control" id="'.$name.'" placeholder="'.$name.'" value="'.$value.'" name="'.$name.'" '.$rq.' '.$rd.' >
    </div>
    ';
}

function drawTextareaFormField($name, $title, $value, $required, $readonly) {
    $star = "";
    $rq = "";
    $rd = "";
    if($required) {
        $star = '<span style="font-size:26px;" class="status text-danger">*</span>';
        $rq = 'required="true"';
    }
    if($readonly) {
        $rd = 'readonly="readonly"'; 
    }
    echo '
        <div class="form-group">
            <label for="'.$name.'">'.$title.' : '.$star.'</label>
            <textarea class="form-control" rows="5" id="'.$name.'" name="'.$name.'" '.$rq.' '.$rd.'>'.$value.'</textarea>
        </div>
    ';
}

function drawSelectFormField($name, $title, $data, $required, $noneOption = false) {
    $star = "";
    $nullOpt = "";
    $req = "";

    if($required) {
        $star = '<span style="font-size:26px;" class="status text-danger">*</span>';
        $req = "required";
    }

    if($noneOption) {
        $nullOpt = '<option value="">--Aucun--</option>';
    }

    echo '
        <div class="form-group">
            <label for="'.$name.'">'.$title.' : '.$star.'</label>
            <select class="form-control" id="'.$name.'" name="'.$name.'" '.$req.'>
            '.$nullOpt.'    
    ';
        foreach($data as $d) {
            echo '<option value="'.$d->value.'">'.$d->name.'</option>';
        }
    echo '
            </select>
        </div>
    ';
}

function drawSelectColorPickerFormText($name, $array) {
    echo "<div class='form-group'>
    <label for='".$name."'>".$name." :</label>
    <select name='colorpicker-regularfont-".$name."'>
    <option value='#5cb85c'>Green</option>
    <option value='#48dbfb'>Blue light</option>
    <option value='#3498db'>Blue</option>
    <option value='#1abc9c'>Turquoise</option>
    <option value='#f1c40f'>Yellow</option>
    <option value='#e67e22'>Orange</option>
    <option value='#ff9ff3'>Pink</option>
    <option value='#e74c3c'>Red</option>
    <option value='#9b59b6'>Purple</option>
    <option value='#bdc3c7'>Gray</option>
    </select>
        <input type='text' class='form-control' id='".$name."' name='".$name."' value='".$array[$name][0]."' >
        <input type='hidden' id='FieldType-".$name."' name='FieldType-".$name."' value='text'>
    </div>";
}

function drawSelectColorPickerFormTextarea($name, $array) {
    echo "<div class='form-group'>
    <label for='".$name."'>".$name." :</label>
    <select name='colorpicker-regularfont-".$name."'>
    <option value='#5cb85c'>Green</option>
    <option value='#48dbfb'>Blue light</option>
    <option value='#3498db'>Blue</option>
    <option value='#1abc9c'>Turquoise</option>
    <option value='#f1c40f'>Yellow</option>
    <option value='#e67e22'>Orange</option>
    <option value='#ff9ff3'>Pink</option>
    <option value='#e74c3c'>Red</option>
    <option value='#9b59b6'>Purple</option>
    <option value='#bdc3c7'>Gray</option>
    </select>
        <textarea class='form-control' id='".$name."' name='".$name."' rows='".count($array[$name])."'>".implode("\n", $array[$name])."</textarea>
        <input type='hidden' id='FieldType-".$name."' name='FieldType-".$name."' value='textarea'>
    </div>";
}

function drawBasicColorPicker($name) {
    echo "<div class='form-group'>
    <label for='".$name."'>".$name." :</label>
    <select name='colorpicker-regularfont-".$name."'>
    <option value='#5cb85c'>Green</option>
    <option value='#48dbfb'>Blue light</option>
    <option selected value='#3498db'>Blue</option>
    <option value='#1abc9c'>Turquoise</option>
    <option value='#f1c40f'>Yellow</option>
    <option value='#e67e22'>Orange</option>
    <option value='#ff9ff3'>Pink</option>
    <option value='#e74c3c'>Red</option>
    <option value='#9b59b6'>Purple</option>
    <option value='#bdc3c7'>Gray</option>
    </select>
    </div>";
}

function drawActionInputField($value) {
    echo '
        <input type="text" class="form-control" id="action" placeholder="action" style="display: none;" name="action" value="'.$value.'">
    ';
}

function drawDatePickerFormField($picker, $name, $title, $date) {

    echo '
    <div class="form-group">
        <label for="'.$name.'">'.$title.' :</label>
        <div class="input-group input-append date" id="'.$picker.'">
            <input type="text" class="form-control" id = "'.$name.'" name="'.$name.'" value='.$date.'>
            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
    </div>
    ';
}

function drawPrimaryPanel($title, $body) {
    echo '
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">'.$title.' :</h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-tower"></i>
                            </span>
                        </div>
                    </div>
                    <div class="panel-body">
                        '.$body.'
                    </div>
                </div>
            </div>
        </div>
    ';
}

function includeLibJS() {
    echo '
    <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
    ';
    //<script src="/'. Conf::$root_dir .'/libjs/tablefilter.js"></script>
}

function includeLibDate($id) {
    echo "
    <script src='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js'></script>
    <script>
    $(document).ready(function() {
        $('#".$id."')
            .datepicker({
                format: 'yyyy-mm-dd',
                startDate: '2017-01-01',
                endDate: '2030-12-31'
            })
            .on('changeDate', function(e) {
                // Revalidate the date field
                $('#dateRangeForm').formValidation('revalidateField', 'date');
            });
        $('#dateRangeForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                date: {
                    validators: {
                        notEmpty: {
                            message: 'The date is required'
                        },
                        date: {
                            format: 'YYYY-MM-DD',
                            min: '2017-01-01',
                            max: '2030-12-31',
                            message: 'The date is not a valid'
                        }
                    }
                }
            }
        });
    });
    </script>
    ";
}

function check($value, $default) {
    if(!isset($value) || empty($value)) {
        return $default;
    }
    return $value;
}

function drawTitle($value) {
    echo '
    <div class="col-lg-12 text-center">
        <h1>'.$value.'</h1>
    </div>
    ';
}

function remove_special_chars($string) {
    // special chars allowed : "-" "_" "."
    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\+=\{\}\[\]\|;:"\<\>,\?\\\]/';
    if(preg_match($pattern, $string)) {
        $string=""; //special chars detected : return empty string
       }
    return $string;
}

<?php

/**
 * 
 * 
 * 
 * @param {string} $
 */
function remove_quotes_and_new_line($text) {
    $text = str_replace('"', "", $text);
    $text = str_replace("'", "", $text);
    $text = str_replace("\n", "", $text);
    $text = str_replace("\r", "", $text);
    return $text;
}

function checkJsonError($string) {
    $json = json_decode($string, true);
    switch (json_last_error()) {
        //check json content
        case JSON_ERROR_NONE:
            echo 'console.log("Aucune erreur");'."\n";
        break;
        case JSON_ERROR_DEPTH:
            echo 'alert("Profondeur maximale atteinte");'."\n";
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo 'alert("Inadéquation des modes ou underflow");'."\n";
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo 'alert("Erreur lors du contrôle des caractères");'."\n";
        break;
        case JSON_ERROR_SYNTAX:
            echo 'alert("Erreur de syntaxe ; JSON malformé");'."\n";
        break;
        case JSON_ERROR_UTF8:
            echo 'alert("Caractères UTF-8 malformés, probablement une erreur d\'encodage");'."\n";
        break;
        default:
            echo 'alert("Erreur inconnue");'."\n";
        break;
    }
    return $json;
}

/**
 * @param {string} step
 * @param {string} hh
 * @param {string} defaultM
 * @param {string} defaultH
 */
function printDataHourSelect($hh, $step) {
    for ($i=0; $i<24; $i++) {
        $val_i = (strlen($i) == 1) ? "0".$i : $i; 
        for($j=0; $j < 60 ; $j = $j + $step) {
            $val_j = (strlen($j) == 1) ? "0".$j : $j; 
            if ( $val_i.$val_j == $hh ) { 
                echo '<option value="'.$val_i.$val_j.'" selected="selected" > '.$val_i.' h '.$val_j.'</option>';
            }   
            else {
                echo '<option value="'.$val_i.$val_j.'"> '.$val_i.' h '.$val_j.'</option>';
            }   
        }   
    }   
}


function printArray($array) {
    echo 'var tr = $("<tr>");'."\n";
    foreach ($array as $val) {
        echo "$('<td>').html('".$val."').appendTo(tr);"."\n";
    }
    echo 'tbody.append(tr);'."\n";
}

/**
 * JSON specific parse functions 
 */

function parseBorgStatusJson($name, $val, $reader) {
    $name = remove_quotes_and_new_line($name);
    $minionid = "";
    if($name == "borg_status") {
        foreach($val as $minionid => $subval) {
            foreach($subval as $jsonfilename => $subsubval) {
                $second_td_displayed = "";
                $third_td_displayed = "";
                isset($jsonfilename) ? $first_td_displayed = remove_quotes_and_new_line(stripcslashes($jsonfilename)) : $first_td_displayed = "key not found";
                if(!is_array($subsubval)) {
                    $second_td_displayed = remove_quotes_and_new_line(stripcslashes($subsubval));
                    if($second_td_displayed == $reader->BORG_BACKUP_NOT_FOUND) {
                        $second_td_displayed = '<span class="label" style="background-color: '.$reader->COLOR_BORG_BACKUP_NOT_FOUND.'" >'.$second_td_displayed.'</span>';
                        $first_td_displayed  = '<span class="label" style="background-color: '.$reader->COLOR_BORG_BACKUP_NOT_FOUND.'" >'.$first_td_displayed.'</span>';
                    } elseif($second_td_displayed == $reader->BORG_BACKUP_FILE_EMPTY) {
                        $second_td_displayed = '<span class="label" style="background-color: '.$reader->COLOR_BORG_BACKUP_FILE_EMPTY.'" >'.$second_td_displayed.'</span>';
                        $first_td_displayed  = '<span class="label" style="background-color: '.$reader->COLOR_BORG_BACKUP_FILE_EMPTY.'" >'.$first_td_displayed.'</span>';
                    }
                } else {
                    foreach($subsubval as $keytype => $details) {
                        if($keytype == "archives") {
                            /*
                            $details is an array of archives
                            The last item corresponds to the last backup
                            */
                            if(is_array($details)) {
                                $start = end($details)["start"];
                                $end = end($details)["time"];
                                $start_timestamp = strtotime($start);
                                $end_timestamp = strtotime($end);
                                $duration = $end_timestamp - $start_timestamp;
                                $minutes = $duration / 60;
                                $duration_str = $minutes;
                                $second_td_displayed = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'">'.$start.'</span>';
                                $third_td_displayed = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'">'.$duration_str.'</span>';
                            } else {
                                $second_td_displayed = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_ERROR.'">Unable to parse archive results...</span>';
                            }
                        }
                    }
                }
                printArray([$minionid,$first_td_displayed,$second_td_displayed,$third_td_displayed]);
            }
        }
    }
}

function parseZpoolJson($name, $val, $reader) {

    $name = remove_quotes_and_new_line($name);
    $val = remove_quotes_and_new_line(stripcslashes($val));
    if($val != "") {
        $subval = "";
        if($val == $reader->ZPOOL_STATUS_SUCCESS) {                    
            $subval = '<span class="label" style="background-color: '.$reader->COLOR_ZPOOL_STATUS_SUCCESS.'" >5: '.$val.'</span>';
        } 
        elseif ($val == $reader->ZPOOL_STATUS_INFO) {
            $subval = '<span class="label" style="background-color: '.$reader->COLOR_ZPOOL_STATUS_INFO.'">4: '.$val.'</span>';
        }
        elseif(strpos($val, $reader->ZPOOL_STATUS_PRIMARY) !== false) {
            $subval = '<span class="label" style="background-color: '.$reader->COLOR_ZPOOL_STATUS_PRIMARY.'">3: '.$val.'</span>';
        }
        else {
            $color = "danger"; 
            $style = "style=\"background-color: #e74c3c\"";
            $num = "1:";
            if(strpos($val, $reader->ZPOOL_STATUS_WARNING) !== false) {
                $color = "warning";
                $style = "style=\"background-color: ".$reader->COLOR_ZPOOL_STATUS_WARNING."\"";
                $num = "2:";
            }
            $val = explode(".", $val);
            $subval = "";
            foreach ($val as $value){
                $subval = $subval .'<span class="label" '.$style.'>'.$num.' '.$value.'</span><br/><br/>';

            }
        }
        printArray([$name, $subval]);
    }
}

function parseDstatesJson($name, $val, $reader) {
    $name = remove_quotes_and_new_line($name);
    $val = remove_quotes_and_new_line($val);
    if($val != "") {
        printArray([$name, $val]);
    }
}

function parseWebsitesStatusJson($name, $val, $reader) {

    $name = remove_quotes_and_new_line($name);
    $val  = remove_quotes_and_new_line($val);
    $name = "<a href=\"".$name."\">".$name."</a>";
    if ($val == null) {
        $val = "0";
    }
    if( $val == $reader->DEFAULT_SUCCESS ) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'">'.$val.'</span>';
    } else if($val == $reader->WEBSITE_CHECKSUMS_AND_STATUS_SUCCESS) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITE_CHECKSUMS_AND_STATUS_SUCCESS.'">'.$val.'</span>';
    } else if(in_array ($val, $reader->WEBSITE_CHECKSUMS_AND_STATUS_WARNING)) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITE_CHECKSUMS_AND_STATUS_WARNING.'">'.$val.'</span>';
    } else {
        $val = '<span class="label" style="background-color: #e74c3c">'.$val.'</span>';
    }
    printArray([$name, $val]);
}

function parseWebsitesChecksumsJson($name, $val, $reader) {
    $name  = remove_quotes_and_new_line($name);
    $val   = remove_quotes_and_new_line($val);
    $value = explode(" - ", $name);
    $name  = "<a href=\"".$value[1]."\">".$value[0]."</a>";
    if ($val == null) {
        $val = "0";
    }
    if( $val == $reader->DEFAULT_SUCCESS ) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'">'.$val.'</span>';
    } else if($val == $reader->WEBSITE_CHECKSUMS_AND_STATUS_SUCCESS) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITE_CHECKSUMS_AND_STATUS_SUCCESS.'">'.$val.'</span>';
    } else if(in_array ($val, $reader->WEBSITE_CHECKSUMS_AND_STATUS_WARNING)) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_WEBSITE_CHECKSUMS_AND_STATUS_WARNING.'">'.$val.'</span>';
    } else {
        $val = '<span class="label" style="background-color: #e74c3c">'.$val.'</span>';
    }
    printArray([$name, $val]);
}

function parseOsversionJson($name, $val, $reader) {
    $name = remove_quotes_and_new_line($name);
    $val = remove_quotes_and_new_line($val);
    if ($val == null) {
        $val = "0";
    }
    if( $val == $reader->DEFAULT_SUCCESS ) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else if(in_array ($val, $reader->OSVERSION_SUCCESS)) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_OSVERSION_SUCCESS.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else if(in_array ($val, $reader->OSVERSION_WARNING)) {
            $val = '<span class="label" style="background-color: '.$reader->COLOR_OSVERSION_WARNING.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else {
        $val = '<span class="label" style="background-color: #e74c3c"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    }
    printArray([$name, $val]);
}

function parseDefaultJson($name, $val, $reader) {
    $name = remove_quotes_and_new_line($name);
    $val = remove_quotes_and_new_line($val);
    if ($val == null) {
        $val = "0";
    }
    if( $val == $reader->DEFAULT_SUCCESS ) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'">'.$val.'</span>';
    } else {
        $val = '<span class="label" style="background-color: '."#e74c3c".'">'.$val.'</span>';
    }
    printArray([$name, $val]);
}

function parseSaltversionJson($name, $val, $reader) {

    $name = remove_quotes_and_new_line($name);
    $val = remove_quotes_and_new_line($val);
    $found = false;

    if ($val == null) {
        $val = "0";
    }

    if( $val == $reader->DEFAULT_SUCCESS ) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_DEFAULT_SUCCESS.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else if(in_array ($val, $reader->SALTVERSION_SUCCESS)) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_SALTVERSION_SUCCESS.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else if(in_array ($val, $reader->SALTVERSION_WARNING)) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_SALTVERSION_WARNING.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else if(in_array ($val, $reader->SALTVERSION_INFO)) {
        $val = '<span class="label" style="background-color: '.$reader->COLOR_SALTVERSION_INFO.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
    } else {
        // looking for version 3003.X if 3003 otherwise false
        foreach ($reader->SALTVERSION as $valueSV) {
            if(strpos($val, $valueSV) !== false) {
                $found = true;
                break;
            }
        }
        foreach ($reader->SALTVERSION_SUCCESS as $valueSV) {
            if(strpos($val, $valueSV) !== false) {
                $found = true;
                break;
            }
        }
        if($found) {
            $val = '<span class="label" style="background-color: '.$reader->COLOR_SALTVERSION_INFO.'"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
        } else {
            $val = '<span class="label" style="background-color: #e74c3c"><i class="fas fa-code-branch"></i>&nbsp;'.$val.'</span>';
        }
    }
    printArray([$name, $val]);
}

function parseServiceJsonArray($name, $service_array, $reader) {
    $tdhtml = [];
    // name == salt minion id
    $name = remove_quotes_and_new_line($name);
    $pgrep = False;
    $html_str_result = "";
    $html_str_changes = "";
    $html_str_service_name = "";
    foreach ($service_array as $daemon_type => $daemonval) {
        if(is_array($daemonval) && $daemon_type == "name") {
            $daemonval = $daemonval[0];
        }
        if($daemon_type == "result") {
            if ($daemonval ==  null) {
                $daemonval = "0";
            } else {
                $daemonval = remove_quotes_and_new_line($daemonval);
            }

            if(is_numeric($daemonval)) {
                $html_str_result = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_TRUE.'">'.$daemonval.'</span></td>';
            } else {
                $html_str_result = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_FALSE.'">'.$daemonval.'</span></td>';
            }
        }
        elseif($daemon_type == "name" && $daemonval != "customservice.status") {
            $daemonval = remove_quotes_and_new_line($daemonval);
             if(is_numeric($daemonval)) {
                $html_str_result = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_TRUE.'">'.$daemonval.'</span></td>';
             } else {
                $html_str_result = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_FALSE.'">'.$daemonval.'</span></td>';
             }
            /*
            Using pgrep in customservice when the service name is not available.
            ...sge for example
            */
            $pgrep = True;
        }
        elseif($daemon_type == "changes") {
            if(array_key_exists("ret", $daemonval)) {
                $ret_daemonval = $daemonval["ret"];
                $ret_daemonval[0] = remove_quotes_and_new_line($ret_daemonval[0]);
                // PATH pour nas-isem
                if($name == "nas-isem") {
                    if($ret_daemonval[0] == "nfs-kernel-server") {
                        $tmp = $service_array["__id__"];

                        if($tmp == "check_nfs_on_nas") {
                            $ret_daemonval[0] = "check_nfs";
                        } elseif($tmp == "nfs-kernel-server__nas-isem") {
                            $ret_daemonval[0] = "nfs-kernel-server";
                        } else {
                            $ret_daemonval[0] = $tmp;
                        }
                    }
                }
                $ret_daemonval[1] = remove_quotes_and_new_line($ret_daemonval[1]);
                if($ret_daemonval[1] == "false" || $ret_daemonval[1] == "")
                    { $ret_daemonval[1] = 0; }
                $html_str_service_name = '<td>'.$ret_daemonval[0].'</td>';

                if($ret_daemonval[1] == "1") {
                    $html_str_changes = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_TRUE.'">'.$ret_daemonval[1].'</span></td>';
                } else {
                    $html_str_changes = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_FALSE.'">'.$ret_daemonval[1].'</span></td>';
                }
            }
            elseif(array_key_exists("customservice.status", $daemonval)) {
                $ret_daemonval = $daemonval["customservice.status"];
                $ret_daemonval[0] = remove_quotes_and_new_line($ret_daemonval[0]);
                $ret_daemonval[1] = remove_quotes_and_new_line($ret_daemonval[1]);
                
                $html_str_service_name = "<td>".$ret_daemonval[0]."</td>";
                if($ret_daemonval[1] != "false" && $ret_daemonval[1] != 0) {
                    $html_str_changes = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_TRUE.'">'.$ret_daemonval[1].'</span></td>';
                } else {
                    if(empty($ret_daemonval[1])) {
                        $ret_daemonval[1] = 0;
                    }
                    $html_str_changes = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_FALSE.'">'.$ret_daemonval[1].'</span></td>';
                }
            }
            elseif(array_key_exists("stdout", $daemonval)) {
                $ret_daemonval = $daemonval["stdout"];
                $ret_daemonval[0] = remove_quotes_and_new_line($ret_daemonval);
                // $pgrep == True
                if(is_numeric($ret_daemonval)) {
                    $html_str_result = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_TRUE.'">'.$ret_daemonval.'</span></td>';
                } else {
                    $html_str_result = '<td><span class="label" style="background-color: '.$reader->COLOR_SERVICE_STATE_FALSE.'">'.$ret_daemonval.'</span></td>';
                    }
                }
            }
        $tdhtml['result'] = $html_str_result;
        $tdhtml['service_name'] = $html_str_service_name;
        $tdhtml['changes'] = $html_str_changes;
    }
    
    printArray([$name, $tdhtml['result'], $tdhtml['service_name'], $tdhtml['changes']]);
    //reinitialize
    $pgrep = False;
}

function getDateTimeTitle($filename) {
    $value = $filename;
    $value = explode("/", $value);
    $values = explode("_", end($value));

    $date = substr($values[0], 0, 4) . "-" . substr($values[0], 4, 2) . "-" . substr($values[0], 6, 2);
    $time = substr($values[1], 0, 2) . "h" . substr($values[1], 2, 2);

    return $date . " " . $time;
}

function get_previous($curH, $curM, $step=30) {
    if(intval($curM) < $step) {
        $curH--;
        $defaultM = strval(60-$step);
    }  
    elseif(intval($curM) > $step) {
        for($i=0; $i < 60 ; $i = $i + $step) {
            if(intval($curM) > $i) {$defaultM = $i; break;}
        }
        $defaultM = strval($defaultM-$step); 
    }
    $new_hh = $curH.$defaultM;
    return $new_hh;
}

function check_files_from_older_versions($JSON_EXPORT_PATH, $curYYMM, $curDD, $dateChoosen, $hh, $data, $find) {
    for ($i = 1; $i <= 10; $i++) {
        // old config
        $filename = $JSON_EXPORT_PATH . $curYYMM . "/". $curDD . "/" . $dateChoosen . "_". substr($hh, 0, 3).strval($i) . $data . ".json";
        if(file_exists($filename)) {
            $find = true;
            break;
        }
    }
    if(!$find) {
        // very old config
        $filename = $JSON_EXPORT_PATH . $curYYMM . "/". $curDD . "/" . $dateChoosen . "_". substr($hh, 0, 2) . $data . ".json";
    }
    return $filename;
}

?>

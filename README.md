# Jsonreader2

Jsonreader v2 is a PHP Web application to parse JSON files.

I am using it for my machines reporting, with a cron + SaltStack outputs in json format, but it can also parse other JSON files (check `include/toolReader.php` on how to do this). For example, I am also using it to view some JSON web reports.

## Origins

I needed a basic WebUI view from SaltStack outputs and JSON is an output available from SaltStack results.

This version 2 is a complete redesign from the MBB team internal project `portail_admin`. It was a part of MBB `portail_admin` application (wrote mainly by my former co-worker [_Jimmy Lopez_](https://github.com/Falindir)), but is now a plugin of MBB `portail_admin` and can be used in a standalone version (default).

The first version was a single file; you can see it [here](https://github.com/remyd1/salt_states/tree/master/monitor_salt_json). In this first version, cron files were written into `/var/www/html/exports/YYYYMM/YYYYMMDD_export-name.json`, then in `/var/www/html/exports/YYYYMM/YYYYMMDD_HH_export-name.json` and `/var/www/html/exports/YYYYMM/YYYYMMDD_HHMM_export-name.json`. In this version 2, files are located, by default, in `/var/www/html/exports/YYYYMM/DD/YYYYMMDD_HH_MM_export-name.json`.

# Examples

There are some screenshots available in the [screenshots](./screenshots/) directory.

![Host status](screenshots/jsonreader2_hosts_status.png "Host status")

## Install

The easiest way is to configure `jsonreader2` on your SaltStack master with a Web server on it and using JSON exports directly.

*Nevertheless, another option would be to mount on the SaltStack master a remote directory that would be used for JSON exports and web display. Finally, another option would be to configure `salt-api`, but I did not try this for that purpose.*

Assuming you are using the easiest way (everything installed on the Salt master).

1. [Required] Clone this repository in your WWW document root and create the exports directory for your json files (web user must have access to the files created by the cron user):

```bash
export_dir=/var/www/html/exports
mkdir -p $export_dir
chmod -R 755 $export_dir
```

2. [Required] Copy `check_salt_json` to `/usr/local/sbin` and set it to be executable by root
3. [Optional] If you are using some additional [jsonreader2 plugins](#Specific-plugin-formulas), edit `check_salt_json`:
    - Uncomment all the plugins you use, following "`# UNCOMMENT NEXT LINE(S) TO USE IT`",
    - If you use the `check_disks` formula, change `TARGET` by a minion ID in `check_salt_json`. The `TARGET` minion is a minion who knows disk smart status of all physical minions through a mine called `mine_disks`,
4. [Required] Add a crontab: `*/30 * * * * /usr/local/sbin/check_salt_json 2>/dev/null`,
    and reload the service:

```bash
service cron reload
```

5. [Required] Install all needed packages for this web application:
  - `php-fpm` and reload your web server:

```bash
# for example, ubuntu/debian with php7.3 and apache:
apt install -y php-fpm
a2enmod proxy_fcgi setenvif
a2enconf php7.3-fpm
systemctl reload apache2
```

  - You also need a mysql/mariadb server:

```bash
apt install -y mariadb-server php-mysql
a2enmod proxy_fcgi setenvif
```

6. [Required] Copy and edit files in `config/` directory:

```bash
cp config/Conf.php.sample config/Conf.php
vi config/Conf.php
cp config/Plugin.php.sample config/Plugin.php
vi config/Plugin.php
```

> `config/Plugin.php`: By default, only the mandatory sections are available. Either as a standalone version or as a plugin version of `portail_admin`, you need to adjust `$SECTIONS` to the jsonreader2 plugins you installed in `config/Plugin.php`.

7. [Required] Install the database using `*.sql` in `sql` directory; create mysql user and set it correctly in `config/Conf.php` and `sql/jsonreader.sql`.

```bash
mysql -uroot -p < sql/jsonreader.sql
mysql -uroot -p jsonreader2 < sql/CReader.sql
mysql -uroot -p jsonreader2 < sql/ReaderTable.sql
mysql -uroot -p jsonreader2 < sql/CReaderPanel.sql
```

> For a jsonreader2 plugin version of `portail_admin`, please check `config/Plugin.php.sample`.

## Using SaltStack Formula

`Jsonreader2` is using some specific SaltStack formula + basic formula (`test.ping`, `service.status`, `osfinger` ...).

## Other JSON reports

To use other JSON reports, you need to clone the plugin (or the SaltStack formula), and then, uncomment or add specific report in `check_salt_json`. Then, add the jsonreader2 plugin title in `config/Plugin.php` (`$SECTION`).

For a new plugin from your own, or an update of jsonreader2, you will need to also add it in the database, and a PHP way to parse the JSON file. Adding it in the database can be done in the WebUI, using the `Configuration` menu item (standalone version, `misc` otherwise).

### Specific plugin formulas

All these plugins need to be set accordingly to your SaltStack configuration.

  - To check disks smart status, take a look at [`check_disks`](https://gitlab.mbb.univ-montp2.fr/saltstack-formulas/check_disks),
  - To check services, take a look at the [`check_services`](https://gitlab.mbb.univ-montp2.fr/saltstack-formulas/check_services) formula,
  - To check D state processes, take a look at [`get_d_states`](https://gitlab.mbb.univ-montp2.fr/saltstack-formulas/get_d_states) formula,
  - To check borgbackup report, take a look at [`borgbackup`](https://gitlab.mbb.univ-montp2.fr/saltstack-formulas/borgbackup) formula.

### Other Json reports

It can also use web reports from [`website_checks`](https://gitlab.mbb.univ-montp2.fr/remy/website_checks). To use it, clone it in `/usr/local/website_checks`.

## Logrotate

The `/var/www/html/exports/` may grow quickly. A `logrotate` file is available in `utils/` directory. If you want to use it, copy it to `/etc/logrotate.d/checkjson` and reload the service.

### Proxying jsonreader2

You may need to install a Web proxy as a web frontend, to avoid direct web connections to your salt server.

This could be easily done with (for example) nginx :

```json
    location ~ "^/my-jsonreader((/?)(|_hosts_status|_services|_disks_status|_dstates|_zpool|_biosdate|_saltversion|_osversion|_kernelversion|_borg_status))$" {
        allow IP.IP.IP.IP;
        allow IP.IP.IP.IP;
        deny all;
        proxy_pass https://<SALT SERVER IP>/<JSONREADER LOCATION>/services/jsonreader.php?data=$3;
    }
```

> With IP.IP.IP.IP the IP of the clients allowed to look at `jsonreader2` reporting page. If you also need nginx authentication, you can take a look [here](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/)

Then, you can access to the `jsonreader2` results with `http(s)://<Proxy IP or FQDN>/my-jsonreader` and for direct tab access, you can, for example, add `/_hosts_status` to the previous URL.
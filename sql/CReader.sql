-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 16 sep. 2021 à 14:19
-- Version du serveur :  10.3.31-MariaDB-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jsonreader2`
--

-- --------------------------------------------------------

--
-- Structure de la table `CReader`
--

CREATE TABLE `CReader` (
  `ID` int(11) NOT NULL,
  `ckey` varchar(255) NOT NULL,
  `cvalue` varchar(255) NOT NULL,
  `FieldType` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `color` varchar(7) DEFAULT '#FFFFFF'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `CReader`
--

INSERT INTO `CReader` (`ID`, `ckey`, `cvalue`, `FieldType`, `color`) VALUES
(1, 'DEFAULT_SUCCESS', '1', 'text', '#5cb85c'),
(2, 'DISKS_USAGE_DANGER', '90', 'text', '#e74c3c'),
(3, 'DISKS_USAGE_WARNING', '75', 'text', '#f1c40f'),
(4, 'DISKS_STATUS_OK', 'OK', 'text', '#5cb85c'),
(5, 'ZPOOL_STATUS_SUCCESS', 'all pools are healthy', 'text', '#5cb85c'),
(6, 'ZPOOL_STATUS_INFO', 'no pools available', 'text', '#ff9ff3'),
(7, 'ZPOOL_STATUS_PRIMARY', 'ZFS modules', 'text', '#e67e22'),
(8, 'ZPOOL_STATUS_WARNING', 'ONLINE', 'text', '#f1c40f'),
(9, 'SALTVERSION_INFO', '2019;3002', 'textarea', '#3498db'),
(10, 'OSVERSION_SUCCESS', 'Ubuntu-18.04;Debian-10;CentOS Linux-7;Raspbian-10;Ubuntu-20.04', 'textarea', '#5cb85c'),
(11, 'WEBSITE_CHECKSUMS_AND_STATUS_SUCCESS', 'OK', 'text', '#5cb85c'),
(12, 'WEBSITE_CHECKSUMS_AND_STATUS_WARNING', 'Found;Permanently', 'textarea', '#f1c40f'),
(13, 'WEBSITES_CERTS_SUCCESS', '5', 'text', '#5cb85c'),
(14, 'WEBSITES_CERTS_WARNING', '1', 'text', '#f1c40f'),
(15, 'WEBSITES_CERTS_STATUS_OK', 'Ok', 'text', '#5cb85c'),
(16, 'SALTVERSION_SUCCESS', '3003.3;3003.2;3003.1;3003', 'textarea', '#5cb85c'),
(17, 'SALTVERSION_WARNING', '3000;3001', 'textarea', '#e67e22'),
(18, 'SERVICE_STATE_FALSE', 'False', 'text', '#e74c3c'),
(19, 'SERVICE_STATE_TRUE', 'True', 'text', '#5cb85c'),
(20, 'BORG_BACKUP_NOT_FOUND', 'No borgbackup report file found!', 'text', '#e74c3c'),
(21, 'BORG_BACKUP_FILE_EMPTY', 'borgbackup report file found but is empty!', 'text', '#e67e22');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `CReader`
--
ALTER TABLE `CReader`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `CReader`
--
ALTER TABLE `CReader`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

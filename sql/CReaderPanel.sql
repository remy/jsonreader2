-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 16 sep. 2021 à 14:20
-- Version du serveur :  10.3.31-MariaDB-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jsonreader2`
--

-- --------------------------------------------------------

--
-- Structure de la table `CReaderPanel`
--

CREATE TABLE `CReaderPanel` (
  `ID` int(11) NOT NULL,
  `CReaderID` int(11) DEFAULT NULL,
  `PanelTitle` varchar(250) COLLATE utf8mb4_bin NOT NULL,
  `PanelPosition` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `CReaderPanel`
--

INSERT INTO `CReaderPanel` (`ID`, `CReaderID`, `PanelTitle`, `PanelPosition`) VALUES
(1, 1, 'Global', 1),
(2, NULL, 'Hosts status', 2),
(3, 18, 'Service', 3),
(4, 2, 'Volume usage', 4),
(5, 3, 'Volume usage', 4),
(6, 4, 'Disks status', 5),
(7, 5, 'Zpool', 6),
(8, 6, 'Zpool', 6),
(9, 7, 'Zpool', 6),
(10, 8, 'Zpool', 6),
(11, NULL, 'D State Processes', 7),
(12, NULL, 'Bios date', 8),
(13, 9, 'Salt Version', 9),
(14, 16, 'Salt Version', 9),
(15, 17, 'Salt Version', 9),
(16, NULL, 'Kernel version', 10),
(17, 10, 'OS version', 11),
(18, 11, 'Websites checksums & status', 12),
(19, 12, 'Websites checksums & status', 12),
(20, 13, 'Website certs', 13),
(21, 14, 'Website certs', 13),
(22, 15, 'Website certs', 13),
(23, 19, 'Service', 3),
(24, 20, 'BorgBackup', 14),
(25, 21, 'BorgBackup', 14);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `CReaderPanel`
--
ALTER TABLE `CReaderPanel`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `CReaderPanel`
--
ALTER TABLE `CReaderPanel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

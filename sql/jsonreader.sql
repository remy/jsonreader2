CREATE DATABASE jsonreader2;
CREATE USER 'jsonreader2'@localhost IDENTIFIED BY 'jsonreader_s3cret2';
GRANT ALL PRIVILEGES ON jsonreader2.* TO 'jsonreader2'@localhost IDENTIFIED BY 'jsonreader_s3cret2';
FLUSH PRIVILEGES;
-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 16 sep. 2021 à 14:20
-- Version du serveur :  10.3.31-MariaDB-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jsonreader2`
--

-- --------------------------------------------------------

--
-- Structure de la table `ReaderTable`
--

CREATE TABLE `ReaderTable` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `head` text NOT NULL,
  `pos` int(11) NOT NULL DEFAULT 9999,
  `jssort` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `ReaderTable`
--

INSERT INTO `ReaderTable` (`ID`, `name`, `title`, `head`, `pos`, `jssort`) VALUES
(1, '_hosts_status', 'Hosts Status', 'Server;Status', 1, '\"pageLength\": -1,             \"order\": [[ 1, \"desc\" ], [0, \"asc\"]]'),
(2, '_services', 'Services', 'Server;Command Result;Service;Status', 2, '\"pageLength\": -1,             \"order\": [[ 3, \"asc\" ], [0, \"asc\"], [2, \"asc\"]]'),
(3, '_disks', 'Disks', 'Server;Mountpoint;Usage [%]', 4, '\"pageLength\": -1,             \"order\": [[ 2, \"desc\" ], [0, \"asc\"]]'),
(4, '_disks_status', 'Disks Status', 'Server;Disk;Status', 5, '\"pageLength\": -1,             \"order\": [[2, \"asc\"]],             \"aoColumns\": [             {                 \"bSortable\": true             },             {                 \"bSortable\": true             },             {                 \"sType\": \"rank\",                 \"bSortable\": true             }]'),
(5, '_zpool', 'Zpool', 'Server;Status', 6, ''),
(6, '_dstates', 'Dstates', 'Server;Status', 3, ''),
(7, '_biosdate', 'Bios date', 'Server;Versions', 7, ''),
(8, '_saltversion', 'Salt version', 'Server;Versions', 8, ''),
(9, '_kernelversion', 'Kernel version', 'Server;Versions', 9, ''),
(10, '_osversion', 'OS version', 'Server;Versions', 10, ''),
(11, '_websites_checksums', 'Website checksums', 'Website;Checksums', 12, ''),
(12, '_websites_status', 'Website status', 'Website;Status', 13, ''),
(13, '_websites_certs', 'Website certs', 'Website;End date;Days before expiration;Status', 11, ''),
(14, '_borg_status', 'Borg', 'Server;Borg File;last backup;duration (mn)', 14, '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ReaderTable`
--
ALTER TABLE `ReaderTable`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ReaderTable`
--
ALTER TABLE `ReaderTable`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php 


require_once(__DIR__.'/../config/Plugin.php');
require_once(__DIR__.'/'.JSR_PATH.'/model/Model.php');

/**
 * ReaderTable model
 */
class ReaderTable extends Model
{

    var $ID = 0;
    var $name = "";
    var $title = "";
    var $head = "";
    var $pos = 9999;
    var $jssort = '';

    public function __construct($ID, $name, $title, $head, $pos, $jssort) {
        $this->ID     = $ID;
        $this->name   = $name;
        $this->title  = $title;
        $this->head   = $head;
        $this->pos    = $pos;
        $this->jssort = $jssort;
    }

    public function escape($db) {
        $this->ID = utf8_decode($db->escape($this->ID));
        $this->name = utf8_decode($db->escape($this->name));
        $this->title = utf8_decode($db->escape($this->title));
        $this->head = utf8_decode($db->escape($this->head));
        $this->pos = utf8_decode($db->escape($this->pos));
        $this->jssort = utf8_decode($db->escape($this->jssort));
    }
    
    public function getInsert() {
        return "INSERT INTO ReaderTable (name, title, head, pos, jssort)
        VALUES ('$this->name', '$this->title', '$this->head', '$this->pos', '$this->jssort');";
    }

    public function getUpdate() {
        return "UPDATE ReaderTable
        SET name='$this->name', title='$this->title', head='$this->head', pos='$this->pos', jssort='$this->jssort'
        WHERE ID = '$this->ID';";
    }

    public function getDelete() {
        return "DELETE FROM ReaderTable
        WHERE ID = '$this->ID';";
    }
    
    public function getHTMLHead() {
        $pieces = explode(";", $this->head);
        $head = "<tr>";
        foreach ($pieces as $h) {
            $head .= "<th>".$h."</th>";
        }
        $head .= "</tr>";
        return $head;
    }
}

?>
<?php


class SuperReader {


    public function __construct($creaders) {

        $DEFAULT_SUCCESS = "Ok;ok;OK;True;true;TRUE;success;succeed;succeeded";
        $DEFAULT_ERROR   = "Error;error;ERROR;Failed;failed;FAILED;False;false;FALSE";
        $DEFAULT_WARNING = "Warning;warning;WARNING";
    
        $COLOR_DEFAULT_SUCCESS = "#5cb85c";
        $COLOR_DEFAULT_ERROR   = "#d32f2f";
        $COLOR_DEFAULT_WARNING = "#f57c00";

        $this->DEFAULT_SUCCESS = $DEFAULT_SUCCESS;
        $this->DEFAULT_ERROR   = $DEFAULT_ERROR;
        $this->DEFAULT_WARNING = $DEFAULT_WARNING;
        $this->COLOR_DEFAULT_SUCCESS = $COLOR_DEFAULT_SUCCESS;
        $this->COLOR_DEFAULT_ERROR   = $COLOR_DEFAULT_ERROR;
        $this->COLOR_DEFAULT_WARNING = $COLOR_DEFAULT_WARNING;

        foreach($creaders as $cr) {
            $ckey = $cr->ckey;
            if($cr->FieldType == "textarea") {
                $arrvals = explode(";", $cr->cvalue);
            } else {
                $arrvals = $cr->cvalue;
            }
            is_numeric($arrvals) ? $this->$ckey = intval($arrvals) : $this->$ckey = $arrvals;
            $colorkey = "COLOR_".$ckey;
            $this->$colorkey = $cr->color;    
        }
    }

}

?>
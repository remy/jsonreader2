<?php

require_once(__DIR__.'/../config/Plugin.php');
//require_once(__DIR__.'/'.JSR_PATH.'/model/Model.php');
require_once(__DIR__.'/SuperReader.php');

/**
 * CReader model
 */
class CReader extends SuperReader
{
    var $ID     = 0;
    var $ckey   = "";
    var $cvalue = "";
    var $FieldType = "text";
    var $color  = "";
    public function __construct($ID, $ckey, $cvalue, $FieldType, $color) {
        $this->ID     = $ID;
        $this->ckey   = $ckey;
        $this->cvalue = $cvalue;
        $this->FieldType = $FieldType;
        $this->color  = $color;
    }
    public function to_array() {
        $result = array(
            $this->ckey,
            $this->cvalue,
            $this->FieldType,
            $this->color
        );
        return $result;
    }
    public function getUpdate() {
        return "UPDATE CReader
        SET cvalue='$this->cvalue', color='$this->color', FieldType='$this->FieldType'
        WHERE ckey = '$this->ckey';";
    }
    public function escape($db) {
        $this->ID = utf8_decode($db->escape($this->ID));
        $this->ckey = utf8_decode($db->escape($this->ckey));
        $this->cvalue = utf8_decode($db->escape($this->cvalue));
        $this->FieldType = utf8_decode($db->escape($this->FieldType));
        $this->color = utf8_decode($db->escape($this->color));
    }
    public function getInsert() {
        return "INSERT INTO CReader (ckey, cvalue, FieldType, color)
        VALUES ('$this->ckey', '$this->cvalue', '$this->FieldType', '$this->color');";
    }
    public function getUpdateByID() {
        return "UPDATE CReader
        SET ckey='$this->ckey', cvalue='$this->cvalue', color='$this->color', FieldType='$this->FieldType'
        WHERE ID = '$this->ID';";
    }
    public function getDelete() {
        return "DELETE FROM CReader
        WHERE ID = '$this->ID';";
    }
    public function getMessage($type) {
        if($type == "create") {
            return "Un objet de type CReader a été ajouté.";
        } elseif ($type == "update") {
            return "Un objet de type CReader a été modifié.";
        } elseif ($type == "delete") {
            return "Un objet de type CReader a été supprimé.";
        } else {
            return "";
        }
    }    
}
<?php

require_once(__DIR__.'/../config/Plugin.php');
//require_once(__DIR__.'/'.JSR_PATH.'/model/Model.php');
require_once(__DIR__.'/SuperReader.php');

/**
 * CReaderPanel model
 */
class CReaderPanel extends SuperReader
{
    var $ID     = 0;
    var $CReaderID   = "";
    var $PanelTitle = "";
    var $PanelPosition = "";
    public function __construct($ID, $CReaderID, $PanelTitle, $PanelPosition) {
        $this->ID            = $ID;
        $this->CReaderID     = $CReaderID;
        $this->PanelTitle    = $PanelTitle;
        $this->PanelPosition = $PanelPosition;
    }
    public function to_array() {
        $result = array(
            $this->CReaderID,
            $this->PanelTitle,
            $this->PanelPosition
        );
        return $result;
    }
    public function escape($db) {
        $this->ID = utf8_decode($db->escape($this->ID));
        $this->CReaderID = utf8_decode($db->escape($this->CReaderID));
        $this->PanelTitle = utf8_decode($db->escape($this->PanelTitle));
        $this->PanelPosition = utf8_decode($db->escape($this->PanelPosition));
    }    
    /*public function getUpdateByID() {
        return "UPDATE CReaderPanel
        SET CReaderID='$this->CReaderID', PanelTitle='$this->PanelTitle', PanelPosition='$this->PanelPosition'
        WHERE ID = '$this->ID';";
    }*/
    public function getUpdate() {
        return "UPDATE CReaderPanel
        SET CReaderID='$this->CReaderID', PanelTitle='$this->PanelTitle', PanelPosition='$this->PanelPosition'
        WHERE ID = '$this->ID';";
    }
    public function getInsert() {
        return "INSERT INTO CReaderPanel
        SET CReaderID='$this->CReaderID', PanelTitle='$this->PanelTitle', PanelPosition='$this->PanelPosition';";
    }
    public function getInsertWithoutCReaderID() {
        return "INSERT INTO CReaderPanel
        SET PanelTitle='$this->PanelTitle', PanelPosition='$this->PanelPosition';";
    }
    public function getDelete() {
        return "DELETE FROM CReaderPanel
        WHERE ID = '$this->ID';";
    }
    public function getMessage($type) {
        if($type == "create") {
            return "Un objet de type CReaderPanel a été ajouté.";
        } elseif ($type == "update") {
            return "Un objet de type CReaderPanel a été modifié.";
        } elseif ($type == "delete") {
            return "Un objet de type CReaderPanel a été supprimé.";
        } else {
            return "";
        }
    }   
}
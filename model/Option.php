<?php

require_once(__DIR__.'/'.JSR_PATH.'/model/Model.php');

/**
 * Option model
 */
class Option extends Model
{
    var $name = "";
    var $value = "";
    public function __construct($name, $value) {
        $this->name     = $name;
        $this->value    = $value;
    }
}

?>